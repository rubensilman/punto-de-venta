contenidoVista = $("#divCuerpoMaster").find("#"+idFuncionalidad);
$(document).ready(function(){
    _token = $("input[name='_token']").val();
    var codigoCliente = null;
    var resultadoArticulos = [];
    var productosSeleccionadosVenta = [];
    var iProductoSeleccionado;
    var indexValidandoArticulo;
    var eliminoProducto = false;
    var resultadoClientes;
    var clienteSeleccionado = null;

    var buscarClienteCodigo = function(e){
        if(e.which == 13){
            codigoCliente =  $(contenidoVista).find("#txtNumeroCliente").blur().val();
            if ( codigoCliente.length == 5 ) {
                var parametros = {};
                parametros.CodigoCliente = $(contenidoVista).find("#txtNumeroCliente").val().trim();
                buscarCliente(parametros);
            }
            else{
                toastr.warning("El número de cliente debe ser de 5 cifras.", "Atención");
                $(contenidoVista).find("#txtNumeroCliente").val("").focus();
            }
        }
    }

    var buscarClientePorNombre = function(){
        var parametros = {};
        parametros.Nombres = $(contenidoVista).find("#txtNombreCliente").val().trim();
        parametros.ApellidoPaterno = $(contenidoVista).find("#txtAPaternoCliente").val().trim();
        parametros.ApellidoMaterno = $(contenidoVista).find("#txtAMaternoCliente").val().trim();
        buscarCliente(parametros);
    }

    var buscarCliente = function(parametros){
        ajaxFunction( parametros , "buscarClienteCriterios", "GET" , "/Catalogos/Clientes" , "JSON", mostrarDatosCliente, limpiarDatosCliente);
    }

    var buscarProductos = function() {
        var parametros = {};
        $(contenidoVista).find(":focus").blur();
        parametros.filtro = $("#ddlFiltroBusquedaProducto").val();
        parametros.criterio = $("#txtCriterioProducto").val();
        ajaxFunction(parametros, 'articulosCriterios', "GET", "/Articulos", "json", resultadoBusquedaArticulo);
    }

    var consultarExistenciaArticulo = function($idArticulo, cantidad, callback, callbackTipo1){
        var parametros = {};
        parametros.IdArticulo = $idArticulo;
        parametros.cantidad = cantidad;
        ajaxFunction(parametros, 'consultarExistenciaPorIdArticulo', "GET", "/ArticuloExistenciaAlmacen", "json", callback, callbackTipo1);
    }

    var eliminarProducto = function(){
        var trSeleccionado = $(this).closest("tr");
        var indexProducto = $(trSeleccionado).index();
        $(trSeleccionado).remove();
        productosSeleccionadosVenta.splice(indexProducto,1);
        if(productosSeleccionadosVenta.length == 0){
            //$(contenidoVista).find("#btnGuardarEntrada").hide();
            $("#tbProductosEdicion > tbody").append("<tr><td colspan='10'>No hay elementos para mostrar</td></tr>");
        }
        if(productosSeleccionadosVenta.length > 0){
            var parametros = {};
            parametros.detalles = productosSeleccionadosVenta;
            ajaxFunction(parametros, "recalcularTotales", "GET", "/Venta", "json", mostrarRecalculoTotales);
        }
        else{
            $("#lblEnganche").text(formato_numero(0, 2, ".", ","));
            $("#lblBonificacion").text(formato_numero(0, 2, ".", ","));
            $("#lblTotal").text(formato_numero(0, 2, ".", ","));
            $(contenidoVista).find("#txtCriterioProducto").focus()
        }
    }

    var iProductoAgregado = function(idProducto){
        var bandera = false;
        var i;
        for ( i = 0; i < productosSeleccionadosVenta.length; i++) {
            if(productosSeleccionadosVenta[i].IdArticulo == idProducto){
                bandera = true;
                return i;
            }
        }
        return bandera ? i : -1;
    }

    var limpiarDatosCliente = function(){
        codigoCliente = null;
        $(contenidoVista).find("#btnBuscarCliente").removeProp("disabled");
        $(contenidoVista).find("#txtNumeroCliente").val("").removeProp("disabled").focus();
        $(contenidoVista).find("#txtNombreCliente").val("").removeProp("disabled");
        $(contenidoVista).find("#txtAPaternoCliente").val("").removeProp("disabled");
        $(contenidoVista).find("#txtAMaternoCliente").val("").removeProp("disabled");
        $(contenidoVista).find("#txtRFCCliente").val("").removeProp("disabled");
        clienteSeleccionado = null;
    }

    var modalProductosOnShow = function(){
        $('#tablaResultadoArticulos > tbody > tr:eq(0)').find(".btnSeleccionarArticulo").focus();
    }

    var mostrarDatosCliente = function(response){
        if(response.datos){
            if(response.datos.length == 1){
                $(contenidoVista).find("#btnBuscarCliente").prop("disabled", "disabled");
                clienteSeleccionado = response.datos[0];
                $(contenidoVista).find("#txtNumeroCliente").val(clienteSeleccionado.Codigo).prop("disabled", "disabled");
                $(contenidoVista).find("#txtNombreCliente").val(clienteSeleccionado.Nombres).prop("disabled", "disabled");
                $(contenidoVista).find("#txtAPaternoCliente").val(clienteSeleccionado.ApellidoPaterno).prop("disabled", "disabled");
                $(contenidoVista).find("#txtAMaternoCliente").val(clienteSeleccionado.ApellidoMaterno).prop("disabled", "disabled");
                $(contenidoVista).find("#txtRFCCliente").val(clienteSeleccionado.RFC).prop("disabled", "disabled");
                mostrarDetalleVenta();
            }
            else{
                $(contenidoVista).find("#txtAMaternoCliente").blur();
                $('#tablaResultadoClientes > tbody').empty();
                resultadoClientes = response.datos;
                for (var i = 0; i < resultadoClientes.length; i++) {
                    var tr = $('<tr>').appendTo('#tablaResultadoClientes > tbody');
                    $('<td>').append(resultadoClientes[i].Nombres).appendTo(tr);
                    $('<td>').append(resultadoClientes[i].ApellidoPaterno).appendTo(tr);
                    $('<td>').append(resultadoClientes[i].ApellidoMaterno).appendTo(tr);
                    $('<td>').append('<button type="button" class="btn btn-primary btnSeleccionarCliente">Seleccionar</button>').appendTo(tr);
                }
                $(contenidoVista).find("#modalResultadoClientes").modal();
            }
        }
    }
    
     var seleccionarCliente = function(){
         var response = {};
         var index = $(this).closest("tr").index();
         response.datos = [resultadoClientes[index]];
         clienteSeleccionado = resultadoClientes[index];
         $(contenidoVista).find("#modalResultadoClientes").modal("hide");
         mostrarDatosCliente(response);
     }

    var mostrarDetalleVenta = function(){
        $("#portletDetalleVenta").slideDown();
        $("#portletDetalleVenta").find(".portlet-body").slideDown();
        desplazarASeccion("#portletDetalleVenta");
        $("#ddlFiltroBusquedaProducto").focus();
    }

    var validarRecalculo = function(e){
        indexValidandoArticulo = $(this).closest("tr").index();
        if(e.data == 1){
            var trArticulo = $('#tbProductosEdicion > tbody').find("tr").eq(indexValidandoArticulo);
            consultarExistenciaArticulo(productosSeleccionadosVenta[indexValidandoArticulo].IdArticulo, $(trArticulo).find(".txtCantidad").val(), recalcularTotalesArticulo, existenciaInsuficiente);
        }
        else{
            recalcularTotalesArticulo();
        }
    }

    var existenciaInsuficiente = function(){
        var trArticulo = $('#tbProductosEdicion > tbody').find("tr").eq(indexValidandoArticulo);
        $(trArticulo).find(".txtCantidad").val(1).trigger("change ");
    }

    var recalcularTotalesArticulo = function(){
        var index = indexValidandoArticulo;
        var productoSeleccionado = productosSeleccionadosVenta[index];
        var trArticulo = $('#tbProductosEdicion > tbody').find("tr").eq(index);
        var parametros = {};
        parametros.IdArticulo = productoSeleccionado.IdArticulo;
        parametros.cantidad =  $(trArticulo).find(".txtCantidad").val();
        ajaxFunction(parametros, "calcularTotalArticulo", "GET", "/Venta", "json", mostrarPrecioArticulo);
    }

    var mostrarPrecioArticulo = function(respuesta){
        var trArticulo = $('#tbProductosEdicion > tbody').find("tr").eq(indexValidandoArticulo);
        $(trArticulo).find("td[data-identificador='preciounitario']").text(formato_numero(respuesta.precioUnitario, 2, ".", ","));
        $(trArticulo).find("td[data-identificador='subtotal']").text(formato_numero(respuesta.precioFinal, 2, ".", ","));
        var cantidad = $(trArticulo).find(".txtCantidad").val();
        productosSeleccionadosVenta[indexValidandoArticulo].PrecioUnitario = respuesta.precioUnitario;
        productosSeleccionadosVenta[indexValidandoArticulo].PrecioFinal = respuesta.precioFinal;
        productosSeleccionadosVenta[indexValidandoArticulo].Cantidad = cantidad;
        var parametros = {};
        parametros.detalles = productosSeleccionadosVenta;
        ajaxFunction(parametros, "recalcularTotales", "GET", "/Venta", "json", mostrarRecalculoTotales);
        
    }

    var mostrarRecalculoTotales = function(respuesta){
        $("#lblEnganche").text(formato_numero(respuesta.totales.enganche, 2, ".", ","));
        $("#lblBonificacion").text(formato_numero(respuesta.totales.bonificacion, 2, ".", ","));
        $("#lblTotal").text(formato_numero(respuesta.totales.totalPagar, 2, ".", ","));
        $(contenidoVista).find("#txtCriterioProducto").focus()
    }

    var resultadoBusquedaArticulo = function( respuesta ){
        resultadoArticulos = respuesta.datos;
        if (resultadoArticulos.length > 0) {
            if ($(contenidoVista).find("#ddlFiltroBusquedaProducto").val() == "Codigo" && resultadoArticulos.length == 1) {
                iProductoSeleccionado = 0;
                seleccionarArticuloBusqueda();
            }
            else{
                $('#tablaResultadoArticulos > tbody').empty();
                for (var i = 0; i < resultadoArticulos.length; i++) {
                    var tr = $('<tr>').appendTo('#tablaResultadoArticulos > tbody');
                    $('<td>').append(resultadoArticulos[i].Codigo1).appendTo(tr);
                    $('<td>').append(resultadoArticulos[i].NombreArticulo).appendTo(tr);
                    $('<td>').append(resultadoArticulos[i].Marca).appendTo(tr);
                    $('<td>').append(resultadoArticulos[i].Modelo).appendTo(tr);
                    $('<td>').append('<button type="button" class="btn btn-primary btnSeleccionarArticulo">Seleccionar</button>').appendTo(tr);
                }
                $(contenidoVista).find("#modalResultadoProductos").modal();
            }
        }
        else{
            toastr.info("No se encontraron productos.", "Información");
            $(contenidoVista).find("#txtCriterioProducto").focus();
        }
    }

    var resultadoConsultaExistencia = function(respuesta){
        seleccionarArticuloBusqueda();
        $(contenidoVista).find("#modalResultadoProductos").modal("hide");
    }

    var seleccionarArticulo = function(){
        iProductoSeleccionado =  $(this).closest("tr").index();
        consultarExistenciaArticulo(resultadoArticulos[iProductoSeleccionado].IdArticulo, 1, resultadoConsultaExistencia);
    }

    var seleccionarArticuloBusqueda = function(){
        var indiceProductoAgregado = iProductoAgregado(resultadoArticulos[iProductoSeleccionado].IdArticulo);
        if (indiceProductoAgregado == -1) {
            if(productosSeleccionadosVenta.length == 0){
                $("#tbProductosEdicion > tbody").empty();
            }
            var botonEliminar = $('<button type="button" class="btn btn-default btnEliminarProducto">Quitar</button>');
            productosSeleccionadosVenta.push(resultadoArticulos[iProductoSeleccionado]);
            var tr = $('<tr>').appendTo('#tbProductosEdicion > tbody');
            //$('<td>').append(resultadoArticulos[iProductoSeleccionado].Codigo1).appendTo(tr);
            $('<td>').append(resultadoArticulos[iProductoSeleccionado].NombreArticulo).appendTo(tr);
            //$('<td>').append(resultadoArticulos[iProductoSeleccionado].Marca).appendTo(tr);
            $('<td>').append(resultadoArticulos[iProductoSeleccionado].Modelo).appendTo(tr);
            $('<td>').append("<input type='number' value='1' class='form-control txtCantidad enterNext' data-enter0='#txtCriterioProducto' data-enter0='.txtDesc"+resultadoArticulos[iProductoSeleccionado].IdArticulo+"'  />").appendTo(tr);
            $('<td data-identificador="preciounitario">').append(formato_numero(resultadoArticulos[iProductoSeleccionado].Precio, 2, ".", ",")).appendTo(tr);
            $('<td data-identificador="subtotal">').appendTo(tr);
            //$('<td>').append("<input type='number' value='0' class='form-control txtDescuentoArticulo txtDesc"+resultadoArticulos[iProductoSeleccionado].IdArticulo+" enterNext' data-enter0='#txtCriterioProducto' />").appendTo(tr);
            //$('<td data-identificador="total">').appendTo(tr);
            $('<td>').append(botonEliminar).appendTo(tr);
            $(tr).find(".txtCantidad").focus().select();
            indexValidandoArticulo = $(tr).index();
            recalcularTotalesArticulo();
            //$(tr).find(".txtDescuentoArticulo").trigger("change");
        }
        else{
            $("#tbProductosEdicion > tbody > tr").eq(indiceProductoAgregado).find(".txtCantidad").focus().select();
            toastr.info("Ya se encuentra agregado el producto.", "Información");
        }
    }

    var calcularPlazos = function(){
        if(productosSeleccionadosVenta.length > 0 && clienteSeleccionado != null){
            var parametros = {};
            parametros.detalles = productosSeleccionadosVenta;
            ajaxFunction(parametros, "calcularPlazos", "GET", "/Venta", "json", mostrarPlazos);
        }
        else{
            toastr.info("Los datos ingresados no son correctos, favor de verificar", "Información");
        }
        
    }

    var mostrarPlazos = function(respuesta){
        $("#portletPlazos").slideDown();
        $("#portletDetalleVenta").slideUp();
        $("#portletDatosCliente").slideUp();
        $('#tbPlazos > tbody').empty();
        var plazos = respuesta.plazos;
        $.each(plazos, function(index, plazo){
            var tr = $("<tr>").appendTo("#tbPlazos > tbody");
            $('<td>').append(index + " ABONOS DE").appendTo(tr);
            $('<td>').append(formato_numero(plazo.ImporteAbono, 2, ".", ",")).appendTo(tr);
            $('<td>').append("TOTAL A PAGAR " + formato_numero(plazo.TotalPagar, 2, ".", ",")).appendTo(tr);
            $('<td>').append("SE AHORRA " + formato_numero(plazo.Ahorro, 2, ".", ",")).appendTo(tr);
            $('<td>').append("<input type='radio' name='Plazo' class='form-control rbPlazo' value='"+ index +"' />").appendTo(tr);
        });
    }

    var ocultarPlazos = function(){
        $("#portletDetalleVenta").slideDown();
        $("#portletDatosCliente").slideDown();
    }

    var guardarVenta = function(){
        var plazo = $(".rbPlazo:checked").val();
        if(plazo){
            var parametros = {};
            parametros.Plazo = plazo;
            parametros.detalles = productosSeleccionadosVenta;
            parametros.IdCliente = clienteSeleccionado.IdCliente;
            ajaxFunction(parametros, "guardarVenta", "POST", "/Venta", "json", ventaFinalizada);
        }
        else{
            toastr.info("Debe seleccionar un plazo para realizar el pago de su compra", "Información");            
        }
    }

    var ventaFinalizada = function(){
        toastr.success("“Bien Hecho, Tu venta ha sido registrada correctamente”", "Información");
        $(contenidoVista).find("#btnIrListadoVentas").trigger("click");
    }

    $(contenidoVista).on("click", "#btnBuscarCliente", buscarClientePorNombre);
    $(contenidoVista).on("click","#btnBuscarProductos", buscarProductos);
    $(contenidoVista).on("click", ".btnEliminarProducto", eliminarProducto);
    $(contenidoVista).on("click" ,".btnSeleccionarArticulo", seleccionarArticulo)
    $(contenidoVista).on("shown.bs.modal", "#modalResultadoProductos", modalProductosOnShow);
    $(contenidoVista).on("change", ".txtCantidad",1 ,validarRecalculo); 
    $(contenidoVista).on("change", ".txtDescuentoArticulo", validarRecalculo); 
    $(contenidoVista).on("click", "#btnSiguientePlazos", calcularPlazos); 
    $(contenidoVista).on("click", "#btnOcultarPlazos", ocultarPlazos); 
    $(contenidoVista).on("click", "#btnGuardarVenta", guardarVenta);
    $(contenidoVista).on("click", "#btnIrListadoVentas", agregarVistaMaster);
    $(contenidoVista).on("keypress", "#txtNumeroCliente", buscarClienteCodigo);
    $(contenidoVista).on("click", "#btnLimpiarDatosCliente", limpiarDatosCliente);
    $(contenidoVista).on("click", ".btnSeleccionarCliente", seleccionarCliente);
    
    $(contenidoVista).find("#txtNumeroCliente").focus();
});

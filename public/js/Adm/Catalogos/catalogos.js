contenidoVista = $("#divCuerpoMaster").find("#"+idFuncionalidad);
$(document).ready(function(){
            
    _token = $("input[name='_token']").val();
    var obtenerFormularioHtml = function(){
        urlActual = $(this).data("ruta");
        ajaxFunction({}, "", "GET", $(this).data("ruta"), "HTML", mostrarFormulario);
    }

    var mostrarFormulario = function(formulario){
        $(".formulario").hide();
        $("#divCuerpoMaster").empty().append(formulario);
    }

    var guardarFormulario = function(){
        var parametros ={};
        parametros.datos = obtenerFormulario("formulario");
        ajaxFunction(parametros, "", "POST", $(this).data("ruta"), "JSON", formularioGuardado);

    }

    var formularioGuardado = function(respuesta){
        toastr.success("Bien Hecho. Se ha sido registrado correctamente", "Exito");
        $(".btnRegresarListado").trigger("click");
    }

    $(".btnMostrarFormulario").on("click", obtenerFormularioHtml);
    $(".btnGuardarFormulario").on("click", guardarFormulario);
    $(".btnRegresarListado").on("click", agregarVistaMaster);
});
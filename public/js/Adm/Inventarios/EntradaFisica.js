contenidoVista = $("#divCuerpoMaster").find("#"+idFuncionalidad);
$(document).ready(function(){
    var _token = $("input[name='_token']").val();
    var entradasPendientes = [];
    var resultadoArticulos = [];
    var productosSeleccionadosEntrada = [];
    var trEntradaSeleccionada = null;
    var entradaACancelar = null;
    var entradaAVer = null;
    var iProductoSeleccionado;
    var buscarEntradasPendientes = function(){
        var parametros = {};
        parametros.idProveedor = $(contenidoVista).find("#ddlProveedor").val();
        ajaxFunction( parametros, "obtenerEntradasPendientes", "GET", "/Inventarios/EntradaFisica" , "JSON", mostrarEntradasPendientes);
    }

    var buscarProductos = function() {
        var parametros = {};
        parametros.filtro = $("#ddlFiltroBusquedaProducto").val();
        parametros.criterio = $("#txtCriterioProducto").val();
        ajaxFunction(parametros, 'articulosCriterios', "GET", "/Articulos", "json", resultadoBusquedaArticulo);
    }

    var cancelarEntrada = function(){
        var parametros = {};
        parametros.IdEntradaFisica = entradaACancelar.IdEntradaFisica;
        parametros.Estatus = "C";
        ajaxFunction(parametros,  "cambiarEstatusEntrada", "POST", "/Inventarios/EntradaFisica", "json", entradaCancelada);
    }

    var capturarSerie = function(){
        var indiceFila = $(this).closest("tr").index();
        if(ValidarCantidades(piezasFaltantes,piezasRecibidas,piezasDañadas,fila)){
            totalSeries = parseInt($(this).closest("tr").find("input.pzasRecibidas").val()) + parseInt($(this).closest("tr").find("input.pzasDañadas").val());
            $('#txtSeries').tagsinput('removeAll');
            $('#txtSeries').tagsinput('destroy');
            $('#txtSeries').tagsinput({
                maxTags: totalSeries,
                trimValue: true
            });
            var seriesInput = $(this).closest("tr").find("input.hdSeries").val().split(',');
            $.each(seriesInput,function(index,value){
                $("#txtSeries").tagsinput('add', value);
            })

            $("#hdFila").val(fila);
            $("#ModalSerial").modal();
        }
    }

    var cerrarDetalleEntrada = function(){
        limpiarDetalleEntrada();
    }

    var limpiarDetalleEntrada = function(){
        $(contenidoVista).find("#txtCriterioProducto").val("");
        $(contenidoVista).find('#ddlProveedorNuevaEntrada').prop("selectedIndex",0);
        $(contenidoVista).find('#ddlFiltroBusquedaProducto').prop("selectedIndex",0);
        $("#tbProductosEdicion > tbody").empty().append("<tr><td colspan='8'>No hay elementos para mostrar</td></tr>");
        $("#btnGuardarEntrada").hide("slow");
        productosSeleccionadosEntrada = [];
    }

    var eliminarProducto = function(){
        var trSeleccionado = $(this).closest("tr");
        var indexProducto = $(trSeleccionado).index();
        $(trSeleccionado).remove();
        productosSeleccionadosEntrada.splice(indexProducto,1);
        if(productosSeleccionadosEntrada.length == 0){
            $(contenidoVista).find("#btnGuardarEntrada").hide();
            $("#tbProductosEdicion > tbody").append("<tr><td colspan='8'>No hay elementos para mostrar</td></tr>");
        }
        $(contenidoVista).find("#txtCriterioProducto").focus()
    }

    var entradaCancelada = function(response){
        toastr.success("Entrada cancelada.","¡Éxito!");
        var index = iEntradaPendiente(entradaACancelar.IdEntradaFisica);
        if (index != -1) {
            $("#tablaEntradasFisicas > tbody > tr:eq("+index+")").remove();
            entradasPendientes.splice(index, 1);
            if (entradasPendientes.length == 0) {
                $("#tablaEntradasFisicas > tbody").append("<tr><td colspan='4'>No hay elementos para mostrar</td></tr>");
            }
        }
    }

    var entradaGuardada = function(response){
        toastr.success("Entrada guardada.", "Éxito");
        limpiarDetalleEntrada();
        $(contenidoVista).find("#portletDetalleEntrada").slideUp();
    }

    var guardarEntrada = function(){
        var parametros = {};
        parametros.detalleEntrada = obtenerDetalle();
        var entrada = {};
        entrada.IdProveedor = $(contenidoVista).find("#ddlProveedorNuevaEntrada").val();
        parametros.entrada = entrada;
        if (parametros.detalleEntrada.length > 0) {
            ajaxFunction(parametros, "guardarEntrada", "POST", "/Inventarios/EntradaFisica", "JSON", entradaGuardada);
        }
    }

    var iProductoAgregado = function(idProducto){
        var bandera = false;
        var i;
        for ( i = 0; i < productosSeleccionadosEntrada.length; i++) {
            if(productosSeleccionadosEntrada[i].IdArticulo == idProducto){
                bandera = true;
                return i;
            }
        }
        return bandera ? i : -1;
    }

    var modalProductosOnShow = function(){
        $('#tablaResultadoArticulos > tbody > tr:eq(0)').find(".btnSeleccionarArticulo").focus();
    }

    var mostrarEntradasPendientes = function(respuesta){
        trEntradaSeleccionada = null;
        entradasPendientes = respuesta.datos;
        $('#tablaEntradasFisicas > tbody').empty();
        if (entradasPendientes.length > 0) {
            for (var i = 0; i < entradasPendientes.length; i++) {
                var botonVer = $("<button class='btn btn-default btnVerEntrada'>Ver</button>");
                var boton = $("<button class='btn btn-info btnRecibirEntrada'>Recibir</button>");
                var botonCancelar = $("<button class='btn btn-warning btnCancelarEntrada'>Cancelar</button>");
                var tr = $('<tr>').appendTo('#tablaEntradasFisicas > tbody');
                $('<td>').append(entradasPendientes[i].Folio).appendTo(tr);
                $('<td>').append(entradasPendientes[i].FechaCreacion).appendTo(tr);
                $('<td>').append(entradasPendientes[i].Nombre).appendTo(tr);
                $('<td>').append([botonVer, "&nbsp;", boton,"&nbsp;", botonCancelar]).appendTo(tr);
            }
        }else{
            toastr.info("No se encontraron entradas pendientes.", "Información");
            $("#tablaEntradasFisicas > tbody").append("<tr><td colspan='4'>No hay elementos para mostrar</td></tr>");
        }
    }

    var nuevaEntradaFisica = function(){
        $(contenidoVista).find("#portletDetalleEntrada").slideDown();
        $(contenidoVista).find("#ddlFiltroBusquedaProducto").focus();
        $(".divEdicionEntrada").show();
        $("#divVerEntrada").hide();
        limpiarDetalleEntrada();
    }

    var resultadoBusquedaArticulo = function( respuesta ){
        resultadoArticulos = respuesta.datos;
        if (resultadoArticulos.length > 0) {
            if ($(contenidoVista).find("#ddlFiltroBusquedaProducto").val() == "Codigo" && resultadoArticulos.length == 1) {
                iProductoSeleccionado = 0;
                seleccionarArticuloBusqueda();
            }
            else{
                $('#tablaResultadoArticulos > tbody').empty();
                for (var i = 0; i < resultadoArticulos.length; i++) {
                    var tr = $('<tr>').appendTo('#tablaResultadoArticulos > tbody');
                    $('<td>').append(resultadoArticulos[i].Codigo1).appendTo(tr);
                    $('<td>').append(resultadoArticulos[i].NombreArticulo).appendTo(tr);
                    $('<td>').append(resultadoArticulos[i].Marca).appendTo(tr);
                    $('<td>').append(resultadoArticulos[i].Modelo).appendTo(tr);
                    $('<td>').append('<button type="button" class="btn btn-primary btnSeleccionarArticulo">Seleccionar</button>').appendTo(tr);
                }
                $(contenidoVista).find("#modalResultadoProductos").modal();
            }
        }
        else{
            toastr.info("No se encontraron productos.", "Información");
            $(contenidoVista).find("#txtCriterioProducto").focus();
        }
    }

    var seleccionarEntadaCancelar = function(){
        var index =  $(this).closest("tr").index();
        entradaACancelar = entradasPendientes[index];
        cancelarEntrada();
    }

    var seleccionarEntradaVer = function(){
        var index =  $(this).closest("tr").index();
        entradaAVer = entradasPendientes[index];
        verEntrada();
    }

    var obtenerDetalle = function(){
        var bandera = true;
        var detalle = [];
        var articulo = {};
        var totalPiezas = 0;
        $("#tbProductosEdicion > tbody > tr").each(function(index){
            articulo = {};
            articulo.CantidadRecibida = $(this).find("input.txtPiezasRecibidas").val();
            articulo.CostoUnitario = $(this).find("input.txtCostoProducto").val();
            articulo.IdArticulo = productosSeleccionadosEntrada[index].IdArticulo;
            detalle.push(articulo);
            // if (totalPiezas > 0) {
                // if ($(this).find("input.hdSeries").val() !== undefined) {
                //     if ($(this).find("input.hdSeries").val() != "") {
                //         articulo.Series =  $(this).find("input.hdSeries").val().split(',');
                //         if(!validaNumSeriesConTotal(articulo.Series,totalPiezas,index))
                //             bandera = false;
                //     }else{
                //         toastr.warning('Favor de introducir las series para el artículo con código: ' + $("#Productos > tbody").find('tr:eq('+ index + ') td:eq(0) > a').text()+'.','¡Advertencia!');
                //         bandera = false;
                //     }
                // }
            // }
        });
        return detalle;
    }

    var seleccionarArticulo = function(){
        iProductoSeleccionado =  $(this).closest("tr").index();
        seleccionarArticuloBusqueda();
        $("#modalResultadoProductos").modal("hide");
    }

    var seleccionarArticuloBusqueda = function(){
        var indiceProductoAgregado = iProductoAgregado(resultadoArticulos[iProductoSeleccionado].IdArticulo);
        if (indiceProductoAgregado == -1) {
            if(productosSeleccionadosEntrada.length == 0){
                $("#tbProductosEdicion > tbody").empty();
            }
            var botonSerie ="";
            if (resultadoArticulos[iProductoSeleccionado].TieneSerie == 1) {
                botonSerie = $('<button type="button" class="btn btn-default btnSeries'+resultadoArticulos[iProductoSeleccionado].IdArticulo+'">Series</button>');
            }
            var botonEliminar = $('<button type="button" class="btn btn-default btnEliminarProducto">Quitar</button>');
            productosSeleccionadosEntrada.push(resultadoArticulos[iProductoSeleccionado]);
            var tr = $('<tr>').appendTo('#tbProductosEdicion > tbody');
            $('<td>').append("<a href='#'>" + resultadoArticulos[iProductoSeleccionado].Codigo1 + "</a>").appendTo(tr);
            $('<td>').append("<a href='#'>" + resultadoArticulos[iProductoSeleccionado].NombreArticulo + "</a>").appendTo(tr);
            $('<td>').append("<a href='#'>" + resultadoArticulos[iProductoSeleccionado].Marca + "</a>").appendTo(tr);
            $('<td>').append("<a href='#'>" + resultadoArticulos[iProductoSeleccionado].Modelo + "</a>").appendTo(tr);
            $('<td>').append('<input class="form-control txtPiezasRecibidas enterNext" data-itope="0" data-enter0=".txtCostoProducto'+resultadoArticulos[iProductoSeleccionado].IdArticulo+'" data-piezas="0"  value="0" type="number" min="0" />').appendTo(tr);
            $('<td>').append('<input class="form-control enterNext txtCostoProducto txtCostoProducto'+resultadoArticulos[iProductoSeleccionado].IdArticulo+'" data-itope="1" data-enter0=".btnSeries'+resultadoArticulos[iProductoSeleccionado].IdArticulo+'" data-enter1="#txtCriterioProducto" data-piezas="0" value="0" type="number" min="0" />').appendTo(tr);
            $('<td>').append(botonSerie).appendTo(tr);
            $('<td>').append(botonEliminar).appendTo(tr);
            $(contenidoVista).find("#btnGuardarEntrada").show("slow");
            $(tr).find(".txtPiezasRecibidas").focus().select();
        }
        else{
            $("#tbProductosEdicion > tbody > tr:eq("+indiceProductoAgregado+")").find(".txtPiezasRecibidas").focus().select();
            toastr.info("Ya se encuentra agregado el producto.", "Información");
        }
    }

    var iEntradaPendiente = function(idEntrada){
        var bandera = false;
        var i;
        for ( i = 0; i < entradasPendientes.length; i++) {
            if(entradasPendientes[i].IdEntradaFisica == idEntrada){
                bandera = true;
                return i;
            }
        }
        return bandera ? i : -1;
    }

    var verEntrada = function(){
        var parametros = {};
        parametros.idEntrada = entradaAVer.IdEntradaFisica;
        ajaxFunction(parametros, "mostrarDetalleEntrada", "GET", "/Inventarios/EntradaFisica", "json", verDetalleEntrada);
    }

    var verDetalleEntrada = function(response){
        $(".divEdicionEntrada").hide();
        $("#divVerEntrada").show();
        $("#divProductoSeleccionado").show();
        var datosEntrada = response.datos.datosEntrada;
        var detalleEntrada = response.datos.detalleEntrada;
        $("#lblFolioEntrada").text( datosEntrada.Folio );
        $("#lblNombreProveedor").text( datosEntrada.NombreProveedor );
        $("#tbProductosEdicion > tbody").empty();
        for (var i = 0; i < detalleEntrada.length; i++) {
            var tr = $('<tr>').appendTo('#tbProductosEdicion > tbody');
            $('<td>').append(detalleEntrada[i].Codigo1).appendTo(tr);
            $('<td>').append(detalleEntrada[i].NombreArticulo).appendTo(tr);
            $('<td>').append(detalleEntrada[i].Marca).appendTo(tr);
            $('<td>').append(detalleEntrada[i].Modelo).appendTo(tr);
            $('<td>').append(detalleEntrada[i].CantidadRecibida).appendTo(tr);
            $('<td>').append(detalleEntrada[i].CostoUnitario).appendTo(tr);
            $('<td>').append("").appendTo(tr);
            $('<td>').append("").appendTo(tr);
        }
        $("#portletDetalleEntrada").slideDown();
        desplazarASeccion("#portletDetalleEntrada");
    }

    var recibirEntrada = function(){
        var index = $(this).closest("tr").index();
        entradaAVer = entradasPendientes[index];
        var parametros = {};
        parametros.IdEntradaFisica = entradasPendientes[index].IdEntradaFisica;
        ajaxFunction(parametros, "recibirEntrada", "POST", "/Inventarios/EntradaFisica", "json", entradaRecibida);
    }

    var entradaRecibida = function(response){
        toastr.success("Entrada recibida.","¡Éxito!");
        var index = iEntradaPendiente(entradaAVer.IdEntradaFisica);
        if (index != -1) {
            $("#tablaEntradasFisicas > tbody > tr:eq("+index+")").remove();
            entradasPendientes.splice(index, 1);
            if (entradasPendientes.length == 0) {
                $("#tablaEntradasFisicas > tbody").append("<tr><td colspan='4'>No hay elementos para mostrar</td></tr>");
            }
        }
    }

    $(contenidoVista).on("click","#btnBuscarEntradas", buscarEntradasPendientes);
    $(contenidoVista).on("click","#btnBuscarProductos", buscarProductos);
    $(contenidoVista).on("click",".btnCancelarEntrada", seleccionarEntadaCancelar);
    $(contenidoVista).on("click",".btnCapturarSerie", capturarSerie);
    $(contenidoVista).on("click", "#btnCerrarDetalleEntrada", cerrarDetalleEntrada);
    $(contenidoVista).on("click", ".btnEliminarProducto", eliminarProducto);
    $(contenidoVista).on("click", "#btnGuardarEntrada", guardarEntrada);
    $(contenidoVista).on("click", ".btnRecibirEntrada", recibirEntrada);
    $(contenidoVista).on("click",".btnVerEntrada", seleccionarEntradaVer);
    $(contenidoVista).on("shown.bs.modal", "#modalResultadoProductos", modalProductosOnShow);
    $(contenidoVista).on("click", "#btnNuevaEntrada", nuevaEntradaFisica);
    $(contenidoVista).on("click" ,".btnSeleccionarArticulo", seleccionarArticulo);
});

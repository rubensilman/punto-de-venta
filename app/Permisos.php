<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Permisos extends Model
{
    use SoftDeletes;
    protected $table = 'permisos';
    protected $primaryKey='IdPermisos';
	protected $fillable = ['IdRoles', 'IdPantallas', 'Lectura', 'Escritura'];
    protected $dates = ['deleted_at'];
    protected $softDelete = true; 
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PrecioArticulo extends Model
{
    protected $primaryKey='IdPrecio';
    protected $fillable = [];
    protected $table = "precios_articulos";
}

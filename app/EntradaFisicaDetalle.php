<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EntradaFisicaDetalle extends Model
{
    protected $table = 'entrada_fisica_detalle';
    protected $primaryKey='IdEntradaFisicaDetalle';
	protected $fillable = ["IdArticulo", "CantidadRecibida", "CostoUnitario"];

    public function constructor($atributos){
        parent::__construct($atributos);
        return ["respuesta" => $this->save()];
    }

    public function darEntradaAlmacen(){
        $existencia = ArticuloExistenciaAlmacen::where("IdAlmacen", 1)->where("IdArticulo", $this->IdArticulo)->first();
        if ($existencia == null) {
            $existencia = new ArticuloExistenciaAlmacen;
            $existencia->IdAlmacen = 1;
            $existencia->IdArticulo = $this->IdArticulo;
        }
        $existencia->Disponible += $this->CantidadRecibida;
        $existencia->Existencia += $this->CantidadRecibida;
        return ["respuesta" => $existencia->save()];
    }
}

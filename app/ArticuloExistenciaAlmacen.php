<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ArticuloExistenciaAlmacen extends Model
{
    protected $table = 'articulos_existencias_almacen';
    protected $primaryKey='IdArticuloExistenciasAlmacen';
    protected $fillable = [];
    
    public function isValid($data){
        $validator = \Validator::make($data, [
            'IdArticulo' => 'required|numeric|exists:cat_articulos,IdArticulo',
            'cantidad' => 'required|numeric|min:1'
        ]);
        if ($validator->fails()) {
            $this->errors = $validator->errors();
            return false;
        }
        else{
            return true;
        }
    }

    public static function consultarExistenciaPorIdArticulo($idArticulo, $cantidad){
        return ArticuloExistenciaAlmacen::
            where("IdArticulo", $idArticulo)
            ->where("Disponible", ">=", $cantidad)
            ->first();
        
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Modulo extends Model
{
    protected $primaryKey='IdModulo';
    protected $fillable = [];
    protected $table = "cat_modulos";
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Funcionalidad extends Model
{
    use SoftDeletes;
    protected $primaryKey='IdFuncionalidad';
    protected $fillable = [];
    protected $table = "cat_funcionalidades_sistema";


}

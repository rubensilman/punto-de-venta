<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EntradaFisica extends Model
{
    protected $table = 'entrada_fisica';
    protected $primaryKey='IdEntradaFisica';
	protected $fillable = ["IdProveedor"];

    public function constructor($atributos){
        parent::__construct($atributos);
        $this->IdUsuarioCreador = \Auth::user()->IdUsuario;
        $this->fill($atributos);
        $this->Estatus = "V";
        $this->IdAlmacen = 1;
        $this->Folio = \App\Folios::construirFolio("EF");
        $this->FechaCreacion = date("Y").'-'.date("m").'-'.date("d");
        return ["respuesta" => $this->save()];
    }

    public function proveedor(){
        return $this->hasOne('\App\Proveedor', 'IdProveedor', 'IdProveedor');
    }

    public function detalleEntrada(){
        return $this->hasMany('\App\EntradaFisicaDetalle', 'IdEntradaFisica', 'IdEntradaFisica');
    }

    public static function obtenerEntradas($criterios){
        $entradas = \DB::table("entrada_fisica AS ef")
            ->join("cat_proveedores AS prov", "prov.IdProveedor", "=", "ef.IdProveedor");
        if (isset($criterios["Estatus"])) {
            $entradas = $entradas->where("ef.Estatus", $criterios["Estatus"]);
        }
        if (isset($criterios["IdProveedor"])) {
            $entradas = $entradas->where("ef.IdProveedor", $criterios["IdProveedor"]);
        }
        $entradas = $entradas->select(
            "ef.IdEntradaFisica",
            "ef.Folio",
            \DB::raw("DATE_FORMAT(ef.FechaCreacion, '%d/%m/%Y') AS FechaCreacion"),
            "prov.Nombre"
        );
        return $entradas->get();
    }

    public function agregarProductos($productos){
        $bandera = true;
        foreach($productos as $producto){
            $entradaDetalle = new EntradaFisicaDetalle;
            $entradaDetalle->IdEntradaFisica = $this->IdEntradaFisica;
            $bandera = $entradaDetalle->constructor($producto)["respuesta"];
            if (!$bandera) {
                $bandera = false;
            }
        }
        return ["respuesta" => $bandera];
    }

    public function obtenerDatosEntrada(){
        $entrada = new \StdClass;
        $entrada->NombreProveedor = $this->proveedor->Nombre;
        $entrada->IdProveedor = $this->proveedor->IdProveedor;
        $entrada->Folio = $this->Folio;
        return $entrada;
    }

    public function obtenerDetalleEntrada(){
        $detalle = \DB::table("entrada_fisica_detalle AS efd")
            ->join("cat_articulos as arts", "arts.IdArticulo", "=", "efd.IdArticulo")
            ->join("cat_modelos as modelo", "modelo.IdModelo", "=", "arts.IdModelo")
            ->join("cat_marcas as marca", "marca.IdMarca", "=", "modelo.IdMarca")
            ->select(
                "arts.Codigo1",
                "arts.NombreArticulo",
                "marca.Marca",
                "modelo.Modelo",
                "efd.CantidadRecibida",
                "efd.CostoUnitario"
            )
            ->where("efd.IdEntradaFisica", $this->IdEntradaFisica)
            ->get();
        return $detalle;
    }

    public function recibirEntrada(){
        $bandera = true;
        $detalleEntrada = $this->detalleEntrada;
        foreach ($detalleEntrada as $detalle) {
            $bandera = $detalle->darEntradaAlmacen()["respuesta"];
            if (!$bandera) {
                $bandera = false;
            }
        }
        if ($bandera) {
            $this->Estatus = "R";
            $bandera = $this->save();
        }
        return ["respuesta"=> $bandera];
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Folios extends Model
{
    protected $table = 'folios';
  	protected $primaryKey = "IdFolio";

  	public static function folio($proceso){
        $folioCD = \App\Folios::where('Proceso',$proceso)->lockForUpdate()->first();
        if ($folioCD->Mes == date("n")) {
            $folioCD->Folio++;
            $folioCD->save();
        }else{
            $folioCD->Folio = 1;
            $folioCD->Mes = date("n");
            $folioCD->save();
        }
        return $folioCD->Folio;
  	}

  public static function construirFolio($proceso){
    $folioCD = \App\Folios::where('Proceso',$proceso)->lockForUpdate()->first();
    if ($folioCD->Mes == date("n")) {
        $folioCD->Folio++;
        $folioCD->save();
    }else{
        $folioCD->Folio = 1;
        $folioCD->Mes = date("n");
        $folioCD->save();
    }
    $folio = $folioCD->Proceso.date("y").date("m").str_pad($folioCD->Folio, 6, "0", STR_PAD_LEFT);
    return $folio;
  }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Cliente extends Model
{
    use SoftDeletes;
    protected $table = "cat_clientes";
    protected $primaryKey = "IdCliente";
    protected $fillable = ["Nombres", "ApellidoPaterno", "ApellidoMaterno", "RFC"];
    protected $softDelete = true;
    
    public function isValid($data){
        $validator = \Validator::make($data, [
            'Nombres' => 'required|max:80',
            'ApellidoPaterno' => 'required|alpha|max:50',
            'ApellidoMaterno' => 'required|alpha|max:50',
            'RFC' => 'required|alpha_num|max:13|min:12',
        ]);
        if ($validator->fails()) {
            $this->errors = $validator->errors();
            return false;
        }
        else{
            return true;
        }
    }

    public static function obtenerClienteCriterios($criterios){
        $clientes = \DB::table("cat_clientes AS cliente");
        if (isset($criterios["CodigoCliente"])) {
            $clientes = $clientes->where("cliente.Codigo", $criterios["CodigoCliente"]);
        }
        if (isset($criterios["Nombres"])) {
            $clientes = $clientes->where("cliente.Nombres", $criterios["Nombres"]);
        }
        $clientes = $clientes->select(
            "cliente.Codigo",
            "cliente.Nombres",
            "cliente.ApellidoPaterno",
            "cliente.ApellidoMaterno",
            "cliente.RFC",
            "cliente.IdCliente"
        );
        return $clientes->get();
    }
}

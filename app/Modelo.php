<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Modelo extends Model
{
    protected $table = "cat_modelos";
    protected $primaryKey = "IdModelo";
    protected $fillable = [];
}

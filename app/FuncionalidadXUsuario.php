<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FuncionalidadXUsuario extends Model
{
    protected $primaryKey='IdFuncionalidadUsuario';
    protected $fillable = [];
    protected $table = "funcionalidades_x_usuario";
}

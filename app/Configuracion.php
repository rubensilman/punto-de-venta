<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Configuracion extends Model
{
    protected $primaryKey='IdConfiguracion';
    protected $fillable = ["TasaFinanciamiento", "PorcEnganche", "PlazoMaximo"];
    protected $table = "configuracion";

    public function isValid($data){
        $validator = \Validator::make($data, [
            'TasaFinanciamiento' => 'required|numeric',
            'PorcEnganche' => 'required|numeric|max:100',
            'PlazoMaximo' => 'required|numeric|max:12',            
        ]);
        if ($validator->fails()) {
            $this->errors = $validator->errors();
            return false;
        }
        else{
            return true;
        }
    }
}

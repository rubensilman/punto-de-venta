<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Articulo extends Model
{
    use SoftDeletes;
    protected $primaryKey='IdArticulo';
    protected $fillable = ["NombreArticulo", "Codigo1", "IdModelo"];
    protected $table = "cat_articulos";

    public function precio(){
        return $this->hasOne('\App\PrecioArticulo', 'IdArticulo', 'IdArticulo');
    }

    public function existencia(){
        return $this->hasOne('\App\ArticuloExistenciaAlmacen', 'IdArticulo', 'IdArticulo');
    }

    public function isValid($data){
        $validator = \Validator::make($data, [
            'Codigo1' => 'required|alpha_num|max:8|unique:cat_articulos',
            'NombreArticulo' => 'required|max:100',
            'IdModelo' => 'required|numeric|exists:cat_modelos,IdModelo',
            'Existencia' => 'required|numeric',
            'Precio' => 'required|numeric|min:0.01',
            
        ]);
        if ($validator->fails()) {
            $this->errors = $validator->errors();
            return false;
        }
        else{
            return true;
        }
    }

    public static function articulosCriterios($criterios){
        $result = \DB::table('cat_articulos AS ca')
            ->join('cat_modelos AS cmo','cmo.IdModelo','=', 'ca.IdModelo')
            ->join('cat_marcas AS cma','cma.IdMarca', '=', 'cmo.IdMarca')
            ->join('precios_articulos AS precio','precio.IdArticulo', '=', 'ca.IdArticulo')
            ->select('ca.NombreArticulo',
                    'ca.Codigo1',
                    'cma.Marca',
                    'cmo.Modelo',
                    'ca.IdArticulo as IdArticulo',
                    'precio.Precio')
            ->whereNull('ca.deleted_at');

            if (isset($criterios["Codigo"]))
                $result = $result->where('ca.Codigo1', $criterios["Codigo"] );
            if (isset($criterios["Marca"]))
                $result = $result->where('cma.Marca', 'like','%'.$criterios["Marca"].'%');
            if (isset($criterios["Modelo"]))
                $result = $result->where('cmo.Modelo', 'like','%'.$criterios["Modelo"].'%');
             if (isset($criterios["Descripcion"]))
                $result = $result->where('ca.NombreArticulo', 'like','%'.$criterios["Descripcion"].'%');
            $result = $result->get();
        return $result;
    }
}

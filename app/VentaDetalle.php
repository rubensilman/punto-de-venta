<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VentaDetalle extends Model
{
    protected $primaryKey='IdVentaDetalle';
    protected $fillable = ["IdArticulo", "Cantidad", "PrecioUnitario", "PrecioFinal", "Precio"];
    protected $table = "ventas_detalle";

    public function isValid($data){
        $validator = \Validator::make($data, [
            'IdArticulo' => 'required|numeric|exists:cat_articulos,IdArticulo',
            'Cantidad' => 'required|numeric|min:1',
            'PrecioUnitario' => 'required|numeric|min:0.01',
            'PrecioFinal' => 'required|numeric|min:0.01',
            'Precio' => 'required|numeric|min:0.01',
        ]);
        if ($validator->fails()) {
            $this->errors = $validator->errors();
            return false;
        }
        else{
            return true;
        }
    }

    public function construct1($attr, $idVenta){
        $this->IdVenta = $idVenta;
        $this->fill($attr);
        $this->save();
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Venta extends Model
{
    protected $primaryKey='IdVenta';
    protected $fillable = ["IdCliente", "Plazo"];
    protected $table = "ventas";

    public function isValid($data, $opcion = 1){
        switch($opcion){
            case 1:
                $validator = \Validator::make($data, [
                    'IdCliente' => 'required|numeric|exists:cat_clientes,IdCliente',
                    'Plazo' => 'required|in:3,6,9,12',
                    'detalles' => 'required|array'
                ]);
                break;
            case 2: 
                $validator = \Validator::make($data, [
                    'IdArticulo' => 'required|numeric|exists:cat_articulos,IdArticulo',
                    'cantidad' => 'required|numeric|min:1',
                ]);
                break;
        }
        
        if ($validator->fails()) {
            $this->errors = $validator->errors();
            return false;
        }
        else{
            return true;
        }
    }

    public function construct1($attr){
        $this->fill($attr);
        $this->Estatus = 'F';
        $this->IdUsuario = \Auth::user()->IdUsuario;
        $this->Folio = "asassa";
        $totales = Venta::calcularTotalesPlazos($attr["detalles"])[$this->Plazo];
        $this->TotalCredito = $totales->TotalPagar;

        $this->save();

        foreach($attr["detalles"] as $detalle){
            $ventaDetalle = new VentaDetalle;
            if($ventaDetalle->isValid($detalle)){
                $ventaDetalle->construct1($detalle, $this->IdVenta);
                $existencia = \App\ArticuloExistenciaAlmacen::where("IdArticulo", $ventaDetalle->IdArticulo)->first();
                $existencia->Existencia -= $ventaDetalle->Cantidad;
                $existencia->Disponible -= $ventaDetalle->Cantidad;
                $existencia->save();
            }
            else{
                return array("respuesta" => false, "errores" => $ventaDetalle->errors->all());
            }  
        }
        return array("respuesta" => true);
    }

    public static function calcularTotales($detalles){
        $total = 0;
        foreach($detalles as $detalle){
            $total += round($detalle["PrecioFinal"] * 100) / 100;
        }
        $configuracion = \App\Configuracion::first();
        $enganche =  round(($configuracion->PorcEnganche / 100) * $total * 100) / 100;
        $bonificacion = round( ($enganche * (($configuracion->TasaFinanciamiento * $configuracion->PlazoMaximo) /100) * 100 ) ) / 100;
        $totalAPAgar = round( ( $total - $enganche - $bonificacion ) * 100 ) / 100 ;
        return ["enganche" => $enganche, "bonificacion" => $bonificacion, "totalPagar" => $totalAPAgar];
    }

    public static function calcularTotalesPlazos($detalles){
        $configuracion = \App\Configuracion::first();
        $totalesVenta = Venta::calcularTotales($detalles);
        $precioContado =  round( ($totalesVenta["totalPagar"] / (1 + (($configuracion->TasaFinanciamiento * $configuracion->PlazoMaximo) / 100))) * 100 ) / 100;
        $plazos = array("3" => new \StdClass, "6" => new \StdClass, "9" => new \StdClass, "12" => new \StdClass);
        foreach($plazos as $plazo => $datos){
            $datos->TotalPagar = round( ($precioContado * (1 + ($configuracion->TasaFinanciamiento * $plazo) / 100) ) * 100 ) / 100;
            $datos->ImporteAbono = round( ($datos->TotalPagar / $plazo ) * 100 ) / 100;
            $datos->Ahorro = round( ($totalesVenta["totalPagar"] - $datos->TotalPagar ) * 100 ) / 100;
        }
        return $plazos;
    }

    public static function obtenerVentasPorEstatus($estatus){
        return \DB::table("ventas")
            ->join("ventas_detalle AS detalle", "detalle.IdVenta", "=", "ventas.IdVenta")
            ->join("cat_clientes AS clientes", "clientes.IdCliente", "=", "ventas.IdCliente")
            ->whereIn("ventas.Estatus", $estatus)
            ->select(
                "ventas.Folio",
                "ventas.TotalCredito",
                "clientes.Codigo",
                \DB::raw("CONCAT(clientes.Nombres, ' ', clientes.ApellidoPaterno, ' ', clientes.ApellidoMaterno) AS NombreCliente"),
                \DB::raw('DATE_FORMAT( ventas.created_at, "%d/%m/%Y") as FechaVenta'))
            ->get();
    } 
}

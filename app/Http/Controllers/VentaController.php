<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class VentaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            if ($request->has("peticion")) {
                $peticion = $request->peticion;
                if ($peticion == "calcularTotalArticulo") {
                    $venta = new \App\Venta;
                    if($venta->isValid($request->all(), 2)){
                        $articulo = \App\Articulo::find($request->IdArticulo);
                        $configuracion = \App\Configuracion::first();
                        $precioUnitario = $articulo->precio->Precio * (1 + ($configuracion->TasaFinanciamiento * $configuracion->PlazoMaximo) /100);
                        $precioFinal = $precioUnitario  * $request->cantidad;
                        return json_encode(array("tipo"=> 0, "precioUnitario" => $precioUnitario, "precioFinal" => $precioFinal));
                    }
                    else{
                        return json_encode(array("tipo" => 5, "mensajes" => $venta->errors->all()));
                    }
                    
                }
                if($peticion == "recalcularTotales"){
                    $totales = \App\Venta::calcularTotales($request->detalles);
                    return json_encode(array("tipo"=> 0, "totales" => $totales));

                }
                if($peticion == "calcularPlazos"){
                    $plazos = \App\Venta::calcularTotalesPlazos($request->detalles);
                    return json_encode(array("tipo"=> 0, "plazos" => $plazos));

                }
            }
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($request->ajax()) {
            if ($request->has("peticion")) {
                $peticion = $request->peticion;
                try{
                    if ($peticion == "guardarVenta") {
                        \DB::beginTransaction();
                        $venta = new \App\Venta;
                        if($venta->isValid($request->all(), 1)){
                            $respuesta = $venta->construct1($request->all());
                            if($respuesta["respuesta"]){
                                \DB::commit();
                                return json_encode(array("tipo"=> 0));
                            }
                            else{
                                \DB::rollback();
                                return json_encode(array("tipo" => 5, "mensajes" => $respuesta["errores"]));
                            }
                            
                        }else{
                            \DB::rollback();
                            return json_encode(array("tipo" => 5, "mensajes" => $venta->errors->all()));
                        }
                    }
                }
                catch(Exception $e){
                    \DB::rollback();
                    return json_encode(array("tipo" => 5, "mensajes" => ["A ocurrido un error al procesar su petición."]));
                }
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

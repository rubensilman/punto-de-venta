<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ArticuloController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            if ($request->has("peticion")) {
                $peticion = $request->peticion;
                if ($peticion == "articulosCriterios") {
                    try{
                        $criterios = [];
                        if ($request->has("filtro")) {
                            $criterios[$request->filtro] = $request->criterio;
                        }
                        $articulos = \App\Articulo::articulosCriterios($criterios);
                        if ($request->wantsJson()) {
                            return json_encode(array("datos"=> $articulos, "tipo"=> 0));
                        }
                        else{
                            return view("Adm/Catalogos/Productos/ListadoProductos", ["productos"=> $articulos]);
                        }
                    }catch (\Exception $e) {
                        report($e);
                        return json_encode(array("datos"=> null, "tipo"=> 5, "mensajes"=> ["Ocurrió un problema al procesar su petición, favor de contactar al administrador del sistema."]));
                    }
                }
            }
            else{
                $productos = \App\Articulo::articulosCriterios([]);
                return view("Adm/Catalogos/Productos/ListadoProductos", ["productos" => $productos]);
            }
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $producto = new \App\Cliente;
        return view("Adm/Catalogos/Productos/FormularioProductos", ["producto" => $producto, "tipo" => 1]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            \DB::beginTransaction();
            $datos = $request->all()["datos"];
            $producto = new \App\Articulo;
            if($producto->isValid($datos)){
                $producto->fill($datos);
                $precio = new \App\PrecioArticulo;
                $precio->Precio = $datos["Precio"];
                $producto->save();
                $precio->IdArticulo = $producto->IdArticulo;
                $precio->save();
                $existencia = new \App\ArticuloExistenciaAlmacen;
                $existencia->IdArticulo = $producto->IdArticulo;
                $existencia->Existencia = $datos["Existencia"];
                $existencia->Disponible = $datos["Existencia"];
                $existencia->save();
                \DB::commit();
                return json_encode(array("tipo" => 0));
            }else{
                \DB::rollback();
                return json_encode(array("tipo" => 5, "mensajes" => $producto->errors->all()));
            }
        }
        catch(Exception $e){
            \DB::rollback();
            return json_encode(array("tipo" => 5, "mensajes" => ["A ocurrido un error al procesar su petición."]));
        }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $producto = \App\Articulo::find($id);
        return view("Adm/Catalogos/Productos/FormularioProductos", ["producto" => $producto, "tipo" => 2]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ArticuloExistenciaAlmacenController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            if ($request->has("peticion")) {
                $peticion = $request->peticion;
                if ($peticion == "consultarExistenciaPorIdArticulo") {
                    $existencia = \App\ArticuloExistenciaAlmacen::consultarExistenciaPorIdArticulo($request->IdArticulo, $request->cantidad);
                    if($existencia != null){
                        if($existencia->isValid($request->all())){
                            return json_encode(array("tipo"=> 0));
                        }
                        else{
                            return json_encode(array("tipo"=> 1, "mensajes" => $existencia->errors->all()));

                        }
                    }
                    return json_encode(array("tipo"=> 1, "mensajes" => ["El artículo seleccionado no cuenta con existencia, favor de verificar"]));
                }
            }
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

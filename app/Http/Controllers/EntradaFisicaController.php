<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class EntradaFisicaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            if ($request->has("peticion")) {
                $peticion = $request->peticion;
                if ($peticion == "obtenerEntradasPendientes") {
                    try{
                        $criterios = [];
                        if ($request->has("idProveedor") && $request->idProveedor != 0) {
                            $criterios["IdProveedor"] = $request->idProveedor;
                        }
                        $criterios["Estatus"] = "V";
                        $entradas = \App\EntradaFisica::obtenerEntradas($criterios);
                        return json_encode(array("datos"=> $entradas, "tipo"=> 0));
                    }catch (\Exception $e) {
                        dd($e);

                        return json_encode(array("datos"=> null, "tipo"=> 5, "mensajes"=> ["Ocurrió un problema al procesar su petición, favor de contactar al administrador del sistema."]));
                    }
                }
                elseif ($peticion == "mostrarDetalleEntrada") {
                    try{
                        $entrada = \App\EntradaFisica::find($request->idEntrada);
                        if ($entrada != null) {
                            $detalleEntrada = $entrada->obtenerDetalleEntrada();
                            $datosEntrada = $entrada->obtenerDatosEntrada();
                            $datos = ["datosEntrada"=> $datosEntrada, "detalleEntrada"=> $detalleEntrada];
                        }
                        return json_encode(array("datos"=> $datos, "tipo"=> 0));
                    }catch (\Exception $e) {
                        return json_encode(array("datos"=> null, "tipo"=> 5, "mensajes"=> ["Ocurrió un problema al procesar su petición, favor de contactar al administrador del sistema."]));
                    }
                }
            }
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($request->ajax()) {
            if ($request->has("peticion")) {
                \DB::beginTransaction();
                try{
                    $peticion = $request->peticion;
                    $response = array();
                    if ($peticion == "guardarEntrada") {
                        if ($request->has("detalleEntrada")) {
                            $entrada = new \App\EntradaFisica;
                            $bandera = true;
                            $bandera = $entrada->constructor($request->entrada)["respuesta"];
                            if($bandera){
                                $bandera = $entrada->agregarProductos($request->detalleEntrada)["respuesta"];
                                if($bandera){
                                    $response["tipo"] = 0;
                                    \DB::commit();
                                }else{
                                    \DB::rollback();
                                    $response["tipo"] = 1;
                                    $response["mensajes"] = ["Ocurrió un problema al procesar su petición, favor de contactar al administrador del sistema."];
                                }
                            }
                        }
                        else{
                            $response["tipo"] = 1;
                            $response["mensajes"] = ["No se encontraron productos en la entrada."];
                        }
                    }
                    elseif ($peticion == "cambiarEstatusEntrada") {
                        $entrada = \App\EntradaFisica::find($request->IdEntradaFisica);
                        if ($entrada != null) {
                            $entrada->Estatus = $request->Estatus;
                            $entrada->save();
                            $response["tipo"] = 0;
                            \DB::commit();
                        }
                    }
                    elseif ($peticion == "recibirEntrada") {
                        $entrada = \App\EntradaFisica::find($request->IdEntradaFisica);
                        if ($entrada != null) {
                            $bandera = $entrada->recibirEntrada()["respuesta"];
                            if ($bandera) {
                                $response["tipo"] = 0;
                                \DB::commit();
                            }else{
                                $response["tipo"] = 1;
                                $response["mensajes"] = ["Ocurrió un problema al procesar su petición, favor de contactar al administrador del sistema."];
                            }
                        }else{
                            $response["tipo"] = 1;
                            $response["mensajes"] = ["No existen la entrada física."];
                        }
                    }
                    return json_encode($response);
                }catch(\Exception $e) {
                    \DB::rollback();
                    return json_encode(array("datos"=> null, "tipo"=> 5, "mensajes"=> ["Ocurrió un problema al procesar su petición, favor de contactar al administrador del sistema."], "Ex" => $e));
                }
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

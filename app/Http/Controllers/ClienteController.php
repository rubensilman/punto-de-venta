<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ClienteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            if ($request->has("peticion")) {
                $peticion = $request->peticion;
                if ($peticion == "buscarClienteCriterios") {
                    try{
                        $clientes = \App\Cliente::obtenerClienteCriterios($request->all());
                        if (count($clientes) >= 1) {
                            return json_encode(array("datos"=> $clientes, "tipo"=> 0));
                        }
                        else{
                            return json_encode(array("datos"=> null, "tipo"=> 1, "mensajes"=> ["No se encontró cliente."]));
                        }
                    }catch (\Exception $e) {
                        report($e);
                        return json_encode(array("datos"=> null, "tipo"=> 5, "mensajes"=> ["Ocurrió un problema al procesar su petición, favor de contactar al administrador del sistema."]));
                    }
                }
            }
            else{
                $clientes = \App\Cliente::obtenerClienteCriterios([]);
                return view("Adm/Catalogos/Clientes/ListadoClientes", ["clientes" => $clientes]);
            }
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $cliente = new \App\Cliente;
        return view("Adm/Catalogos/Clientes/FormularioCliente", ["cliente" => $cliente, "tipo" => 1]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            \DB::beginTransaction();
            $cliente = new \App\Cliente;
            $datos = $request->all()["datos"];
            if($cliente->isValid($datos)){
                $cliente->fill($datos);
                $CodigoMax = \App\Cliente::select('Codigo')->orderBy('Codigo','desc')->withTrashed()->lockForUpdate()->first();
                if($CodigoMax == null)
                {
                    $cliente->Codigo = 10000;
                } 
                else
                {
                    $cliente->Codigo = $CodigoMax->Codigo + 1;
                }
                $cliente->save();
                \DB::commit();
                return json_encode(array("tipo" => 0));
            }
            else{
                \DB::rollback();
                return json_encode(array("tipo" => 5, "mensajes" => $cliente->errors->all()));
            }
        }
        catch(Exception $e){
            \DB::rollback();
            return json_encode(array("tipo" => 5, "mensajes" => ["A ocurrido un error al procesar su petición."]));

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $cliente = \App\Cliente::find($id);
        return view("Adm/Catalogos/Clientes/FormularioCliente", ["cliente" => $cliente, "tipo" => 2]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

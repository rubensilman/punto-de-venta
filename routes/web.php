<?php

Route::get("/", function(){
    return view("welcome");
});
Route::auth();
Route::group(['middleware' => ['auth']],function(){
    Route::get('/Inicio', function () {
        return view('Adm/inicio');
    });
    Route::resource("/Inventarios/EntradaFisica", "EntradaFisicaController");
    Route::resource("/Catalogos/Clientes", "ClienteController");
    Route::resource("/vistas/Catalogos/Articulos", "ArticuloController");
    Route::resource("/vistas/Catalogos/Clientes", "ClienteController");
    Route::resource("/vistas/Catalogos/Configuracion", "ConfiguracionController");
    Route::resource("/Articulos", "ArticuloController");
    Route::resource("/ArticuloExistenciaAlmacen", "ArticuloExistenciaAlmacenController");
    Route::resource("/Venta", "VentaController");
    Route::prefix('vistas')->group(function() {
        Route::get('{modulo}/{funcionalidad}', function ($modulo, $funcionalidad) {
            try {
                if(Request::ajax()){
                    return view("Adm/".$modulo."/".$funcionalidad);
                }
                else{
                    return redirect('/Inicio');
                }
            } catch (\Exception $e) {
                abort(404);
            }
        });
    });
});

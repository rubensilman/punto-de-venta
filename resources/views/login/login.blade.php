﻿<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:400,400italic,600,600italic,800,800italic">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Oswald:400,300,700">
    {!! Html::style("/css/global/bootstrap.min.css" )!!}
    {!! Html::style("/css/global/mvpready-admin.css" )!!}
    <title>Sistema de ventas</title>
    <style>
        body
        {
            background-color:#f1f1f1;
        }
        .btn-primary
        {
            background-color:#ffc000;
        }
    </style>
</head>
<body>
        <div style="margin-top:50px;">
            <div class="account-wrapper">

                <div class="account-body">


                    <img style="width:70%; margin-top:-8px;" src="{{ 'data:image/jpeg;base64,'.base64_encode( \Storage::get( 'public/logos/phpLogo.png' ) ) }}">
                    <br>
                    <h3>Sistema de administración</h3>
                    <br>
                    <h5>Por favor ingrese usuario y contraseña.</h5>

                    <div class="form account-form">
                        <form role='form' method='POST' action="{!! url('/login') !!}" >
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="form-group">
                            {!! Form::label('userId', 'Usuario:', array('class' => 'control-label')) !!}
                            {!! Form::text('NombreUsuario', null, ['class' => 'form-control','placeholder'=>"Usuario"]) !!}
                        </div>
                        <!-- /.form-group -->

                        <div class="form-group">
                            {!! Form::label('password', 'Contraseña:', array('class' => 'control-label')) !!}
                            {!! Form::password('password',['class' => 'form-control','placeholder'=>"Contraseña"]) !!}
                        </div>
                        <!-- /.form-group -->

                        <!-- div class="form-group clearfix">
                            <div class="pull-left">
                                <label class="checkbox-inline">
                                    <input type="checkbox" class="" value="" tabindex="3">
                                    <small>Recordarme</small>
                                </label>
                            </div>

                            <div class="pull-right">
                                <small><a href="./account-forgot.html">¿Olvidó su contraseña?</a></small>
                            </div>
                        </div>
                        <!-- /.form-group -->

                        <div class="form-group">
                            <button type="submit" class="btn btn-primary btn-block btn-lg" tabindex="4">
                                Ingresar &nbsp; <i class="fa fa-play-circle"></i>
                            </button>
                        </div>
                        <!-- /.form-group -->

                    </form>
                    </div>


                </div>
                <!-- /.account-body -->

                <div class="account-footer">
                </div>
                <!-- /.account-footer -->

            </div>
        </div>
</body>
</html>

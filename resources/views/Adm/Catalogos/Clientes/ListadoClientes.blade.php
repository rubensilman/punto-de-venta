{!!Html::script('/js/Adm/Catalogos/catalogos.js')!!}
<div class="formulario">
<div class="row">
    <h3>Catalogo de clientes</h3>
    <td><a class="btn btn-default btnMostrarFormulario" data-ruta="{{ '/vistas/Catalogos/Clientes/create'}}">Nuevo Cliente</a></td>
</div>
<div class=row>
    <table id="tablaResultadoArticulos" class="table table-striped table-bordered table-hover tablaResultados">
        <thead>
            <tr>
                <th>Codigo</th>
                <th>Nombres</th>
                <th>ApellidoPaterno</th>
                <th>Apellido Materno</th>
                <th>RFC</th>
                <th>Acción</th>
            </tr>
        </thead>
        <tbody>
            @foreach($clientes as $cliente)
                <tr>
                    <td>{{ $cliente->Codigo }}</td>
                    <td>{{ $cliente->Nombres }}</td>
                    <td>{{ $cliente->ApellidoPaterno }}</td>
                    <td>{{ $cliente->ApellidoMaterno }}</td>
                    <td>{{ $cliente->RFC }}</td>
                    <td><a class="btn btn-default btnMostrarFormulario" data-ruta="{{ '/vistas/Catalogos/Clientes/'.$cliente->IdCliente}}">ver</a></td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>
</div>
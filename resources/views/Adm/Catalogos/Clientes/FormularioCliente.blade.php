{!!Html::script('/js/Adm/Catalogos/catalogos.js')!!}
{!! csrf_field() !!}
<div class="row">
    @if($tipo == 2)
        <div class="col-sm-3">
            <div class="form-group">
                <label for="Codigo" class="control-label">Codigo</label>
                <input type="text" data-formulario="formulario" {!! ($tipo == 1 ? "" : "readonly") !!} class="form-control" value="{{ old('Codigo', $cliente->Codigo)}}" id="Codigo" placeholder="Codigo" name="Codigo">
            </div>
        </div>
    @endif
    <div class="col-sm-3">
        <div class="form-group">
            <label for="Nombres" class="control-label">Nombres</label>
            <input type="text" data-formulario="formulario" {!! ($tipo == 1 ? "" : "readonly") !!} class="form-control" value="{{ old('Nombres', $cliente->Nombres)}}" id="Nombres" placeholder="Nombres" name="Nombres">
        </div>
    </div>
    <div class="col-sm-3">
        <div class="form-group">
            <label for="ApellidoPaterno" class="control-label">Apellido paterno</label>
            <input type="text"data-formulario="formulario" {!! ($tipo == 1 ? "" : "readonly") !!} class="form-control" value="{{ old('Nombres', $cliente->ApellidoPaterno)}}" name="ApellidoPaterno">
        </div>
    </div>
    <div class="col-sm-3">
        <div class="form-group">
            <label for="ApellidoMaterno" class="control-label">Apellido Materno</label>
            <input type="text" data-formulario="formulario" {!! ($tipo == 1 ? "" : "readonly") !!} class="form-control" value="{{ old('Nombres', $cliente->ApellidoMaterno)}}" name="ApellidoMaterno">
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-3">
        <div class="form-group">
            <label for="RFC" class="control-label">RFC</label>
            <input type="text" data-formulario="formulario" {!! ($tipo == 1 ? "" : "readonly") !!} class="form-control" value="{{ old('Nombres', $cliente->RFC)}}" name="RFC">
        </div>
    </div>
</div>
    
<div class="row">
    <div class="col-sm-offset-3 col-sm-3">
        <div class="form-group">
            <label for="RFC" class="control-label">&nbsp;</label>
            <button data-urlvista="/Catalogos/Clientes" data-idfuncionalidad="CatalogoClientes" data-nombrefuncionalidad="Nueva Venta" class="btn btn-default btnRegresarListado">Regresar</button>
        </div>
    </div>
    @if($tipo == 1)
    <div class="col-sm-3">
        <div class="form-group">
            <label class="control-label">&nbsp;</label>
            <button class="btn btn-success btnGuardarFormulario" data-ruta="{{ '/vistas/Catalogos/Clientes'}}">Guardar</button>
        </div>
    </div>
    @endif
</div>
{!!Html::script('/js/Adm/Catalogos/catalogos.js')!!}
{!! csrf_field() !!}
<div class="row">
    <div class="col-sm-3">
        <div class="form-group">
            <label for="NombreArticulo" class="control-label">Nombre Articulo</label>
            <input type="text" data-formulario="formulario" {!! ($tipo == 1 ? "" : "readonly") !!} class="form-control" value="{{ old('NombreArticulo', $producto->NombreArticulo)}}" name="NombreArticulo">
        </div>
    </div>
    <div class="col-sm-3">
        <div class="form-group">
            <label for="Codigo1" class="control-label">Codigo</label>
            <input type="text" data-formulario="formulario" {!! ($tipo == 1 ? "" : "readonly") !!} class="form-control" value="{{ old('Codigo1', $producto->Codigo1)}}" name="Codigo1">
        </div>
    </div>
    <div class="col-sm-3">
        <div class="form-group">
            <label for="Precio" class="control-label">Modelo</label>
            <select data-formulario="formulario" {!! ($tipo == 1 ? "" : "disabled") !!} class="form-control" name="IdModelo">
            <?php $modelos =  \App\Modelo::get(); ?>
            @foreach($modelos as $modelo)
                <option {!! ($tipo == 2 && $producto->IdModelo == $modelo->IdModelo ? "selected" : "") !!} value="{!! $modelo->IdModelo !!}" >{!! $modelo->Modelo !!}</option>
            @endforeach
            </select>
        </div>
    </div>
    <div class="col-sm-3">
        <div class="form-group">
            <label for="Precio" class="control-label">Precio</label>
            <input type="text"data-formulario="formulario" {!! ($tipo == 1 ? "" : "readonly") !!} class="form-control" name="Precio" value="{{ isset($producto) && $producto->IdArticulo != null ? $producto->precio->Precio : '' }}">
        </div>
    </div>
    <div class="col-sm-3">
        <div class="form-group">
            <label for="Existencia" class="control-label">Existencia</label>
            <input type="text" data-formulario="formulario" {!! ($tipo == 1 ? "" : "readonly") !!} class="form-control" name="Existencia" value="{{ isset($producto) && $producto->IdArticulo != null ? $producto->existencia->Disponible : '' }}">
        </div>
    </div>
</div>
    
<div class="row">
    <div class="col-sm-offset-3 col-sm-3">
        <div class="form-group">
            <label class="control-label">&nbsp;</label>
            <button data-urlvista="/Catalogos/Articulos" data-idfuncionalidad="CatalogoArticulos" data-nombrefuncionalidad="Catalogo de articulos" class="btn btn-default btnRegresarListado">Regresar</button>
        </div>
    </div>
    @if($tipo == 1)
    <div class="col-sm-3">
        <div class="form-group">
            <label class="control-label">&nbsp;</label>
            <button class="btn btn-success btnGuardarFormulario" data-ruta="{{ '/vistas/Catalogos/Articulos'}}">Guardar</button>
        </div>
    </div>
    @endif
</div>
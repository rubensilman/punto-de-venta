{!!Html::script('/js/Adm/Catalogos/catalogos.js')!!}
<div class="formulario">
    <div class="row">
        <h3>Catalogo de Productos</h3>
        <td><a class="btn btn-default btnMostrarFormulario" data-ruta="{{ '/vistas/Catalogos/Articulos/create'}}">Nuevo Articulo</a></td>
    </div>
    <div class=row>
        <table id="tablaResultadoArticulos" class="table table-striped table-bordered table-hover">
            <thead>
                <tr>
                    <th>Código</th>
                    <th>Descripción</th>
                    <th>Marca</th>
                    <th>Modelo</th>
                    <th>Acción</th>
                </tr>
            </thead>
            <tbody>
                @foreach($productos as $producto)
                    <tr>
                        <td>{{ $producto->Codigo1 }}</td>
                        <td>{{ $producto->NombreArticulo }}</td>
                        <td>{{ $producto->Marca }}</td>
                        <td>{{ $producto->Modelo}}</td>
                        <td><a class="btn btn-default btnMostrarFormulario" data-ruta="{{ '/vistas/Catalogos/Articulos/'.$producto->IdArticulo}}">Ver</a></td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>


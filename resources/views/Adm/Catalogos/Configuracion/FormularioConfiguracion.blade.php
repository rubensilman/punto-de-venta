{!!Html::script('/js/Adm/Catalogos/catalogos.js')!!}
{!! csrf_field() !!}
<?php
    $tipo = 1;
?>
<div class="row">
    <div class="col-sm-3">
        <div class="form-group">
            <label for="TasaFinanciamiento" class="control-label">Tasa Financiamiento</label>
            <input type="text" data-formulario="formulario" {!! ($tipo == 1 ? "" : "readonly") !!} class="form-control" value="{{ $configuracion != null ?  $configuracion->TasaFinanciamiento : ''}}" name="TasaFinanciamiento">
        </div>
    </div>
    <div class="col-sm-3">
        <div class="form-group">
            <label for="PorcEnganche" class="control-label">% Enganche</label>
            <input type="text" data-formulario="formulario" {!! ($tipo == 1 ? "" : "readonly") !!} class="form-control" value="{{ $configuracion != null ?  $configuracion->PorcEnganche : ''}}" name="PorcEnganche">
        </div>
    </div>
    
    <div class="col-sm-3">
        <div class="form-group">
            <label for="PlazoMaximo" class="control-label">Plazo Máximo</label>
            <input type="text" data-formulario="formulario" {!! ($tipo == 1 ? "" : "readonly") !!} class="form-control" value="{{ $configuracion != null ?  $configuracion->PlazoMaximo : ''}}" name="PlazoMaximo">
        </div>
    </div>
</div>
    
<div class="row">
    @if($tipo == 1)
    <div class="col-sm-3">
        <div class="form-group">
            <label class="control-label">&nbsp;</label>
            <button class="btn btn-success btnGuardarFormulario" data-ruta="{{ '/vistas/Catalogos/Configuracion'}}">Guardar</button>
        </div>
    </div>
    @endif
</div>
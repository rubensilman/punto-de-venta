<div id="VentasNuevaVenta">
    {!! csrf_field() !!}
    {!!Html::script('/js/Adm/Ventas/NuevaVenta.js?1.0')!!}
    <h2 class="">Nueva Venta</h2>
    <div class="form-group">
    </div>
    <div class="row">
        <div class="portlet portlet-default" id="portletDatosCliente">
            <div class="portlet-header">
                <h4 class="portlet-title">
                    <u>Datos del cliente</u>
                    {!! Form::button('Ocultar', array('class' => 'btn btn-default btnDesplegar', 'style' => 'float: right')) !!}
                </h4>
            </div>

            <div class="portlet-body portletDatosCliente">
                <div class="row">
                    <div class="col-md-2">
                        <div class="form-group">
                            {!! Form::label('txtNumeroCliente', 'Numero de cliente:', array('class' => 'control-label')) !!}
                            {!! Form::input('number', null,"", ['class' => 'form-control', 'id' => 'txtNumeroCliente']) !!}
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-2">
                        <div class="form-group">
                            {!! Form::label('txtNombreCliente', 'Nombre del cliente:', array('class' => 'control-label')) !!}
                            {!! Form::text('', null, ['class' => 'form-control enterNext', 'id' => 'txtNombreCliente', 'data-enter0'=>'#txtAPaternoCliente']) !!}
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            {!! Form::label('txtAPaternoCliente', 'Apellido paterno:', array('class' => 'control-label')) !!}
                            {!! Form::text('', null, ['class' => 'form-control enterNext', 'id' => 'txtAPaternoCliente', 'data-enter0'=>'#txtAMaternoCliente']) !!}
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            {!! Form::label('txtAMaternoCliente', 'Apellido materno:', array('class' => 'control-label')) !!}
                            {!! Form::text('', null, ['class' => 'form-control btnEnterClick', 'id' => 'txtAMaternoCliente', 'data-selector'=>'#btnBuscarCliente']) !!}
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            {!! Form::label('txtRFCCliente', 'RFC:', array('class' => 'control-label')) !!}
                            {!! Form::text('', null, ['class' => 'form-control', 'id' => 'txtRFCCliente']) !!}
                        </div>
                    </div>
                    <div class="col-sm-1">
                            {!! Form::button('Buscar', array('id' => 'btnBuscarCliente' , 'class' => 'btn btn-default')) !!}
                    </div>
                    <div class="col-sm-1">
                            {!! Form::button('Limpiar', array('id' => 'btnLimpiarDatosCliente' , 'class' => 'btn btn-default')) !!}
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="portlet portlet-default" style="display: none;" id="portletDetalleVenta">
            <div class="portlet-header">
                <h4 class="portlet-title">
                    <u>Productos</u>
                    {!! Form::button('Ocultar', array("id"=>"btnOcultarDetalleVenta", 'class' => 'btn btn-default btnDesplegar', 'style' => 'float: right')) !!}
                </h4>
            </div>
            <div class="portlet-body">
                <div class="row divEdicionEntrada">
                    <div class="col-md-2">
                        <div class="form-group">
                            {!! Form::label('txtFiltroProducto', 'Filtro:', array('class' => 'control-label')) !!}
                            {!!Form::select("ddlFiltro",array("Codigo"=> "Codigo", "Descripcion"=>"Descripción", "Marca"=>"Marca", "Modelo"=>"Modelo"),"Codigo", array("class"=>"form-control enterNext", "data-itope"=>"0", "data-enter0"=>"#txtCriterioProducto", "id" => "ddlFiltroBusquedaProducto"))!!}
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            {!! Form::label('txtCriterioProducto', 'Criterio:', array('class' => 'control-label')) !!}
                            {!! Form::text('', null, ['class' => 'form-control btnEnterClick', 'id' => 'txtCriterioProducto', 'data-selector'=>'#btnBuscarProductos']) !!}
                        </div>
                    </div>
                    <div class="col-sm-1">
                            {!! Form::button('Buscar', array('id' => 'btnBuscarProductos' , 'class' => 'btn btn-default')) !!}
                    </div>
                    <div class="col-sm-offset-6 col-sm-1">
                            {!! Form::button('Guardar', array('id' => 'btnGuardarVenta' , 'class' => 'btn btn-lg btn-success', "style"=>"display:none;")) !!}
                    </div>
                </div>
                <div class="row" id="divVerEntrada">
                    <div class="form-group">
                        <div style="display:inline-block" >
                            {!! Form::label('', "Folio:" , array('class' => 'control-label')) !!}
                            <h3><span class="label label-info" id="lblFolioEntrada" ></span></h3>
                        </div>
                        <div style="display:inline-block" >
                            {!! Form::label('', "Proveedor:" , array('class' => 'control-label')) !!}
                            <h3><span class="label label-info" id="lblNombreProveedor" ></span></h3>
                        </div>
                    </div>
                </div>
                <div class="row">

                    <table id="tbProductosEdicion" class="table table-striped table-bordered table-hover">
                        <thead>
                            <tr>

                                <th>Descripción</th>
                                <th>Modelo</th>
                                <th>Cantidad.</th>
                                <th>Precio U.</th>
                                <th>Total</th>
                                <th>Acción</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
                <div class="row">
                    <div class="col-sm-3">
                        <label>Enganche</label>
                        <h1><span id="lblEnganche" class="label label-primary">$0.00</span></h1>
                    </div>
                    <div class="col-sm-3">
                        <label>Bonificacion Enganche</label>
                        <h1><span id="lblBonificacion" class="label label-primary">$0.00</span></h1>
                    </div>
                    <div class="col-sm-3">
                        <label>Total</label>
                        <h1><span id="lblTotal" class="label label-primary">$0.00</span></h1>
                    </div>
                    <div class="col-sm-3">
                        <button id="btnSiguientePlazos" class="btn btn-success btn-lg" style="margin-top: 20px;">Siguiente</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="portlet portlet-default" style="display: none;" id="portletPlazos">
            <div class="portlet-header">
                <h4 class="portlet-title">
                    <u>Plazos</u>
                    {!! Form::button('Ocultar', array("id"=>"btnOcultarPlazos", 'class' => 'btn btn-default btnCerrar', 'style' => 'float: right')) !!}
                </h4>
            </div>
            <div class="portlet-body">
                <div class="row">
                    <table id="tbPlazos" class="table table-striped table-bordered table-hover">
                        <thead>
                            <tr>
                                <th colspan="5" style="text-align: center;">Número plazos</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
                <div class="row">
                    <button id="btnGuardarVenta" class="btn btn-success">Guardar</button>
                    <button id="btnIrListadoVentas" style="display:none;" data-urlvista="/Ventas/HistorialVentas" data-idfuncionalidad="VentasHistorialVentas" data-nombrefuncionalidad="Hitorial de Ventas" class="btn btn-success">Guardar</button>
                </div>
            </div>
        </div>
    </div>

    <div id="modalResultadoProductos" class="modal fade">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    {!! Form::button('&times;', array('class' => 'close', 'data-dismiss' => 'modal', 'aria-hidden' => 'true')) !!}
                    <h3 class="modal-title">Resultado búsqueda de productos:</h3>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class='col-sm-12'>
                            <table id="tablaResultadoArticulos" class="table table-striped table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th>Código</th>
                                        <th>Descripción</th>
                                        <th>Marca</th>
                                        <th>Modelo</th>
                                        <th>Acción</th>
                                    </tr>
                                </thead>
                                <tbody></tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    {!! Form::button('Cerrar', array('class' => 'btn btn-default', 'data-dismiss' => 'modal', 'aria-hidden' => 'true')) !!}
                </div>
            </div>
        </div>
    </div>
    <div id="modalResultadoClientes" class="modal fade">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    {!! Form::button('&times;', array('class' => 'close', 'data-dismiss' => 'modal', 'aria-hidden' => 'true')) !!}
                    <h3 class="modal-title">Resultado búsqueda de clientes:</h3>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class='col-sm-12'>
                            <table id="tablaResultadoClientes" class="table table-striped table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th>Nombre</th>
                                        <th>ApellidoPaterno</th>
                                        <th>ApellidoMaterno</th>
                                        <th>Acción</th>
                                    </tr>
                                </thead>
                                <tbody></tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    {!! Form::button('Cerrar', array('class' => 'btn btn-default', 'data-dismiss' => 'modal', 'aria-hidden' => 'true')) !!}
                </div>
            </div>
        </div>
    </div>

</div>

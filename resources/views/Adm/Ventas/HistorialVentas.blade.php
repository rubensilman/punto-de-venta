<script>
    contenidoVista = $("#divCuerpoMaster").find("#"+idFuncionalidad);
        $(document).ready(function(){
            $(contenidoVista).on("click", "#btnNuevaVenta", agregarVistaMaster);
    });
</script>
<div id="VentasHistorialVentas">
    {!! csrf_field() !!}
    <h2 class="">Historial de ventas</h2>
    <div class="form-group">
        <button id="btnNuevaVenta" data-urlvista="/Ventas/NuevaVenta" data-idfuncionalidad="VentasNuevaVenta" data-nombrefuncionalidad="Nueva Venta" class="btn btn-success">Nueva Venta</button>
    </div>
    <div class="row">
        <div class="portlet portlet-default">
            <div class="portlet-header">
                <h4 class="portlet-title">
                    <u>Ventas activas</u>
                    <button id="btnIrListadoVentas" style="display:none;" data-urlvista="/Ventas/HistorialVentas" data-idfuncionalidad="VentasHistorialVentas" data-nombrefuncionalidad="Hitorial de Ventas" class="btn btn-success">Guardar</button>

                    {!! Form::button('Ocultar', array('class' => 'btn btn-default btnDesplegar', 'style' => 'float: right')) !!}
                </h4>
            </div>

            <div class="portlet-body">
                <div class="row">
                    <table id="tbProductosEdicion" class="table table-striped table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>Folio venta</th>
                                <th>Clave Cliente</th>
                                <th>Nombre</th>
                                <th>Total</th>
                                <th>Fecha</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $ventasFinalizadas = \App\Venta::obtenerVentasPorEstatus(["F"]); ?>
                            @foreach($ventasFinalizadas as $venta)
                                <tr>
                                    <td>{!! $venta->Folio !!}</td>
                                    <td>{!! $venta->Codigo !!}</td>
                                    <td>{!! $venta->NombreCliente !!}</td>
                                    <td>{!! $venta->TotalCredito !!}</td>
                                    <td>{!! $venta->FechaVenta !!}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

</div>
<div id="InventariosEntradaFisica">
    {!! csrf_field() !!}
    {!!Html::script('/js/Adm/Inventarios/EntradaFisica.js?1.0')!!}
    <h2 class="">Entrada Física</h2>
    <div class="form-group">
        {!! Form::button('Nueva entrada', array('class' => 'btn btn-default btnIrASeccion','data-selectorseccion'=>'#portletDetalleEntrada', 'id' => 'btnNuevaEntrada')) !!}
    </div>
    <div class="row">
        <div class="portlet portlet-default">
            <div class="portlet-header">
                <h4 class="portlet-title">
                    <u>Entradas fisicas</u>
                    {!! Form::button('Mostrar', array('class' => 'btn btn-default btnDesplegar btnEntradasFisicas', 'style' => 'float: right')) !!}
                </h4>
            </div>

            <div class="portlet-body entradasFisicas" style="display: none;">
                <div class="row">
                    <div class="col-md-2">
                        <div class="form-group">
                            {!! Form::label('', 'Proveedor:', array('class' => 'control-label')) !!}
                            <select class="form-control" id="ddlProveedor">
                                <?php $proveedores = \App\Proveedor::select('IdProveedor','Nombre')->get(); ?>
                                <option value="0">Todos</option>
                                @foreach($proveedores as $proveedor)
                                    <option value="{{ $proveedor->IdProveedor }}">{{ $proveedor->Nombre }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-1">
                            {!! Form::button('Buscar', array('id' => 'btnBuscarEntradas' , 'class' => 'btn btn-default')) !!}
                    </div>
                </div>
                <table id="tablaEntradasFisicas" class="table table-striped table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>Folio</th>
                            <th>Fecha</th>
                            <th>Proveedor</th>
                            <th>Acción</th>
                        </tr>
                    </thead>
                    <tbody>
                        <td colspan="4">No hay datos para mostrar</td>
                    </tbody>
                </table>
            </div>
            <!-- /.portlet-body -->
        </div>
        <!-- /.portlet -->
    </div>

    <div class="row">
        <div class="portlet portlet-default" style="display: none;" id="portletDetalleEntrada">
            <div class="portlet-header">
                <h4 class="portlet-title">
                    <u>Detalle de entrada física</u>
                    {!! Form::button('Cerrar', array("id"=>"btnCerrarDetalleEntrada", 'class' => 'btn btn-default btnCerrar', 'style' => 'float: right')) !!}
                </h4>
            </div>
            <!-- /.portlet-header -->
            <div class="portlet-body">
                <div class="row divEdicionEntrada">
                    <div class="col-md-2">
                        <div class="form-group">
                            {!! Form::label('', 'Proveedor:', array('class' => 'control-label')) !!}
                            <select class="form-control" id="ddlProveedorNuevaEntrada">
                                <?php $proveedores = \App\Proveedor::select('IdProveedor','Nombre')->get(); ?>
                                @foreach($proveedores as $proveedor)
                                    <option value="{{ $proveedor->IdProveedor }}">{{ $proveedor->Nombre }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row divEdicionEntrada">
                    <div class="col-md-2">
                        <div class="form-group">
                            {!! Form::label('txtFiltroProducto', 'Filtro:', array('class' => 'control-label')) !!}
                            {!!Form::select("ddlFiltro",array("Codigo"=> "Codigo", "Descripcion"=>"Descripción", "Marca"=>"Marca", "Modelo"=>"Modelo"),"Codigo", array("class"=>"form-control enterNext", "data-itope"=>"0", "data-enter0"=>"#txtCriterioProducto", "id" => "ddlFiltroBusquedaProducto"))!!}
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            {!! Form::label('txtCriterioProducto', 'Criterio:', array('class' => 'control-label')) !!}
                            {!! Form::text('', null, ['class' => 'form-control btnEnterClick', 'id' => 'txtCriterioProducto', 'data-selector'=>'#btnBuscarProductos']) !!}
                        </div>
                    </div>
                    <div class="col-sm-1">
                            {!! Form::button('Buscar', array('id' => 'btnBuscarProductos' , 'class' => 'btn btn-default')) !!}
                    </div>
                    <div class="col-sm-offset-6 col-sm-1">
                            {!! Form::button('Guardar', array('id' => 'btnGuardarEntrada' , 'class' => 'btn btn-lg btn-success', "style"=>"display:none;")) !!}
                    </div>
                </div>
                <div class="row" id="divVerEntrada">
                    <div class="form-group">
                        <div style="display:inline-block" >
                            {!! Form::label('', "Folio:" , array('class' => 'control-label')) !!}
                            <h3><span class="label label-info" id="lblFolioEntrada" ></span></h3>
                        </div>
                        <div style="display:inline-block" >
                            {!! Form::label('', "Proveedor:" , array('class' => 'control-label')) !!}
                            <h3><span class="label label-info" id="lblNombreProveedor" ></span></h3>
                        </div>
                    </div>
                </div>
                <div class="row">

                    <table id="tbProductosEdicion" class="table table-striped table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>Código</th>
                                <th>Descripción</th>
                                <th>Marca</th>
                                <th>Modelo</th>
                                <th>Pzas. a recibir</th>
                                <th>Costo</th>
                                <th>Serie</th>
                                <th>Acción</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div id="modalResultadoProductos" class="modal fade">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    {!! Form::button('&times;', array('class' => 'close', 'data-dismiss' => 'modal', 'aria-hidden' => 'true')) !!}
                    <h3 class="modal-title">Resultado búsqueda de productos:</h3>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class='col-sm-12'>
                            <table id="tablaResultadoArticulos" class="table table-striped table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th>Código</th>
                                        <th>Descripción</th>
                                        <th>Marca</th>
                                        <th>Modelo</th>
                                        <th>Acción</th>
                                    </tr>
                                </thead>
                                <tbody></tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    {!! Form::button('Cerrar', array('class' => 'btn btn-default', 'data-dismiss' => 'modal', 'aria-hidden' => 'true')) !!}
                </div>
            </div>
        </div>
    </div>


    <div id="ModalSerial" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            {!! Form::button('&times;', array('class' => 'close', 'data-dismiss' => 'modal', 'aria-hidden' => 'true')) !!}
            <h4 class="modal-title">Capturar número de serie</h4>
        </div>
        <div class="modal-body">
                <div class="form-group">
                    {!! Form::hidden(null,null, array('id' => 'hdFila')) !!}
                    {!! Form::label('', 'Capture el número de serie:', array('class' => 'control-label')) !!}

                    {!! Form::input('', '', '', ['class' => 'form-control','placeholder'=> 'Capturar número de serie', 'id' => 'txtSeries', 'data-role' => 'tagsinput']) !!}
                </div>
            </div>
        <div class="modal-footer">
            {!! Form::button('Regresar', array('class' => 'btn btn-default', 'data-dismiss' => 'modal')) !!}
            {!! Form::button('Guardar', array('class' => 'btn btn-primary', 'id' => 'btnGuardarSeries')) !!}
        </div>
    </div>
    <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
    </div><!-- /.modal -->

    <div id="ModalSerialEditar" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            {!! Form::button('&times;', array('class' => 'close', 'data-dismiss' => 'modal', 'aria-hidden' => 'true')) !!}
            <h4 class="modal-title">Capturar número de serie</h4>
        </div>
        <div class="modal-body">
                <div class="form-group">
                    {!! Form::hidden(null,null, array('id' => 'hdFilaEditar')) !!}
                    {!! Form::label('', 'Capture el número de serie:', array('class' => 'control-label')) !!}

                    {!! Form::input('', '', '', ['class' => 'form-control','placeholder'=> 'Capturar número de serie', 'id' => 'txtSeriesEditar', 'data-role' => 'tagsinput']) !!}
                </div>
            </div>
        <div class="modal-footer">
            {!! Form::button('Regresar', array('class' => 'btn btn-default', 'data-dismiss' => 'modal')) !!}
            {!! Form::button('Guardar', array('class' => 'btn btn-primary', 'id' => 'btnGuardarSeriesEditar')) !!}
        </div>
    </div>
    <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
    </div><!-- /.modal -->

</div>

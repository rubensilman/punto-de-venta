<?php
    $permisos = \Auth::user()->funcionalidades();
    $modulos = \App\Modulo::All();
?>
<!DOCTYPE html>
<html lang="es-mx" class="no-js" xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" >
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <script>
        var ruta = "{!! url(''); !!}";
    </script>
    <!-- Google Font: Open Sans -->
     <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:400,400italic,600,600italic,800,800italic">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Oswald:400,300,700">

    <!-- Font Awesome CSS -->
    {!!Html::style('/css/global/fontawesome/css/font-awesome.min.css')!!}

    <!-- Bootstrap CSS -->
    {!!Html::style('/css/global/bootstrap/dist/css/bootstrap.min.css')!!}

    <!-- Plugin CSS -->
    {!!Html::style('/css/global/select2/dist/css/select2.min.css')!!}
    {!!Html::style('/css/global/select2-bootstrap-theme/dist/select2-bootstrap.min.css')!!}
    {!!Html::style('/css/global/jquery-icheck/skins/minimal/_all.css')!!}


    <!-- App CSS -->
    {!!Html::style('/css/global/mvpready-admin.css')!!}

    <!-- <link rel="stylesheet" href="./css/custom.css')!!} -->

    <!-- Core JS -->
    {!!Html::script('/css/global/jquery/dist/jquery.js')!!}
    {!!Html::script('/css/global/bootstrap/dist/js/bootstrap.min.js')!!}
    {!!Html::script('/js/global/toastr.min.js')!!}
    {!!Html::script('/js/global/jquery.isloading.min.js')!!}

    {!!Html::script('/js/global/')!!}
    {!!Html::script('/js/global/jquery.slimscroll.min.js')!!}
    {!!Html::script('/js/global/mvpready-core.js')!!}
    {!!Html::script('/js/global/mvpready-helpers.js')!!}
    {!!Html::script('/js/global/mvpready-admin.js')!!}
    {!!Html::script('/js/global/generic.js')!!}

    {!!Html::style('/css/global/toastr.css')!!}


    <!--TOASTR -->
    @yield("head")


    <!-- Favicon -->
    <link rel="shortcut icon" href="{!! url('favicon.ico')!!}">
    <title>@yield("title")</title>

</head>
<body class=" " onload="deshabilitaRetroceso()">
    <div id="wrapper">
        <header class="navbar" role="banner">
            <div class="container">
                <div class="navbar-header">
                    <button class="navbar-toggle" type="button" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <i class="fa fa-cog"></i>
                    </button>
                    <a href=" {!! url('InicioSAVIM') !!}" class="navbar-brand navbar-brand-img">
                        {!!Html::image( '/include/img/logoChico.png', null, array( "width"=>"55", "height" => "55" ) )!!}
                    </a>
                </div>
                <!-- /.navbar-header -->
                <nav class="collapse navbar-collapse" role="navigation">
                    <ul class="nav navbar-nav navbar-left">
                        <li class="dropdown navbar-notification">
                            <a href="./page-notifications.html" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="fa fa-bell navbar-notification-icon"></i>
                                <span class="visible-xs-inline">&nbsp;Notificaciones</span>
                                <b class="badge badge-primary">3</b>
                            </a>
                            <div class="dropdown-menu">
                                <div class="dropdown-header">&nbsp;Notificaciones</div>
                                <div class="notification-list">
                                    <a href="./page-notifications.html" class="notification">
                                        <span class="notification-icon"><i class="fa fa-cloud-upload text-primary"></i></span>
                                        <span class="notification-title">Titulo 1</span>
                                        <span class="notification-description">Praesent dictum nisl non est sagittis luctus.</span>
                                        <span class="notification-time">20 minutes ago</span>
                                    </a>
                                    <!-- / .notification -->
                                    <a href="./page-notifications.html" class="notification">
                                        <span class="notification-icon"><i class="fa fa-ban text-secondary"></i></span>
                                        <span class="notification-title">Titulo 2</span>
                                        <span class="notification-description">Lorem ipsum dolor sit amet, consectetur adipisicing elit...</span>
                                        <span class="notification-time">20 minutes ago</span>
                                    </a>
                                    <!-- / .notification -->
                                    <a href="./page-notifications.html" class="notification">
                                        <span class="notification-icon"><i class="fa fa-warning text-tertiary"></i></span>
                                        <span class="notification-title">Titulo 3</span>
                                        <span class="notification-description">Praesent dictum nisl non est sagittis luctus.</span>
                                        <span class="notification-time">20 minutes ago</span>
                                    </a>
                                    <!-- / .notification -->
                                    <a href="./page-notifications.html" class="notification">
                                        <span class="notification-icon"><i class="fa fa-ban text-danger"></i></span>
                                        <span class="notification-title">Titulo 4</span>
                                        <span class="notification-description">Lorem ipsum dolor sit amet, consectetur adipisicing elit...</span>
                                        <span class="notification-time">20 minutes ago</span>
                                    </a>
                                    <!-- / .notification -->
                                </div>
                                <!-- / .notification-list -->
                                <a href="./page-notifications.html" class="notification-link">Ver todas..</a>
                            </div>
                            <!-- / .dropdown-menu -->
                        </li>
                        <li class="dropdown navbar-notification">
                            <a href="./page-notifications.html" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="fa fa-envelope navbar-notification-icon"></i>
                                <span class="visible-xs-inline">&nbsp;Mensajes</span>
                            </a>
                            <div class="dropdown-menu">
                                <div class="dropdown-header">Mensajes</div>
                                <div class="notification-list">
                                    <a href="./page-notifications.html" class="notification">
                                        <div class="notification-icon">
                                            {!!Html::image('/include/global/img/avatars/avatar-3-md.jpg')!!}
                                        </div>
                                        <div class="notification-title">Nuevo Mensaje</div>
                                        <div class="notification-description">Praesent dictum nisl non est sagittis luctus.</div>
                                        <div class="notification-time">Hace 20 minutos</div>
                                    </a>
                                    <!-- / .notification -->
                                    <a href="./page-notifications.html" class="notification">
                                        <div class="notification-icon">
                                            {!!Html::image('/include/global/img/avatars/avatar-3-md.jpg')!!}
                                        </div>
                                        <div class="notification-title">Nuevo Mensaje</div>
                                        <div class="notification-description">Lorem ipsum dolor sit amet, consectetur adipisicing elit...</div>
                                        <div class="notification-time">Hace 20 minutos</div>
                                    </a>
                                    <!-- / .notification -->
                                    <a href="./page-notifications.html" class="notification">
                                        <div class="notification-icon">
                                            {!!Html::image('/include/global/img/avatars/avatar-4-md.jpg')!!}
                                        </div>
                                        <div class="notification-title">Nuevo Mensaje</div>
                                        <div class="notification-description">Lorem ipsum dolor sit amet, consectetur adipisicing elit...</div>
                                        <div class="notification-time">Hace 20 minutos</div>
                                    </a>
                                    <!-- / .notification -->
                                    <a href="./page-notifications.html" class="notification">
                                        <div class="notification-icon">
                                             {!!Html::image('/include/global/img/avatars/avatar-5-md.jpg')!!}
                                        </div>
                                        <div class="notification-title">New Message</div>
                                        <div class="notification-description">Lorem ipsum dolor sit amet, consectetur adipisicing elit...</div>
                                        <div class="notification-time">Hace 20 minutos</div>
                                    </a>
                                    <!-- / .notification -->
                                </div>
                                <!-- / .notification-list -->
                                <a href="./page-notifications.html" class="notification-link">Ver todos..</a>
                            </div>
                            <!-- / .dropdown-menu -->
                        </li>
                        <li class="dropdown navbar-notification empty">
                            <a href="./page-notifications.html" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="fa fa-warning navbar-notification-icon"></i>
                                <span class="visible-xs-inline">&nbsp;&nbsp;Alerts</span>
                            </a>
                            <div class="dropdown-menu">
                                <div class="dropdown-header">Alertas</div>
                                <div class="notification-list">
                                    <h4 class="notification-empty-title">No hay alertas.</h4>
                                    <p class="notification-empty-text">Check out what other makers are doing on Explore!</p>
                                </div>
                                <!-- / .notification-list -->
                                <a href="./page-notifications.html" class="notification-link">View All Alerts</a>
                            </div>
                            <!-- / .dropdown-menu -->
                        </li>
                    </ul>
                    <ul class="nav navbar-nav navbar-right">
                        <li>
                            <a>{!! \Auth::user()->Nombre. " ". \Auth::user()->ApellidoPaterno !!}</a>
                        </li>
                        <li class="dropdown navbar-profile">
                            <a class="dropdown-toggle" data-toggle="dropdown" href="javascript:;">
                                <img src="data:image/jpg;base64,{!! base64_encode(\Auth::user()->ImagenPerfil) !!}" alt="" class="navbar-profile-avatar" />
                                <span class="visible-xs-inline">@peterlandt &nbsp;</span>
                                <i class="fa fa-caret-down"></i>
                            </a>
                            <ul class="dropdown-menu" role="menu">
                                <li>
                                    <a href="{!! route('logout') !!}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                        <i class="fa fa-sign-out"></i>
                                        &nbsp;&nbsp;Salir
                                    </a>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">{{ csrf_field() }}</form>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </nav>
            </div>
            <!-- /.container -->
        </header>
        <div class="mainnav">
            <div class="container">
                <a class="mainnav-toggle" data-toggle="collapse" data-target=".mainnav-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <i class="fa fa-bars"></i>
                </a>
                <nav id="nav" class="collapse mainnav-collapse" role="navigation">
                    <ul class="mainnav-menu">

                    @foreach($modulos as $modulo)
                            <?php
                                $bandera = false;
                                $subModulos = $modulos->where("IdModuloPadre", $modulo->IdModulo);
                                $bandera = true;
                                $idSubmodulos = $subModulos->pluck("IdModulo")->all();
                                $idsModFunc = $permisos->pluck("IdModulo")->all();
                                $result = array_intersect($idSubmodulos, $idsModFunc);
                                
                            ?>

                            @if( ($permisos->where("IdModulo", $modulo->IdModulo)->isNotEmpty() || !empty($result)) && $modulo->IdModuloPadre == null)
                                <li class="dropdown">
                                    <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown">
                                        {!! $modulo->Nombre !!}
                                        <i class="mainnav-caret"></i>
                                    </a>
                                    <ul class="dropdown-menu" role="menu">
                                        @foreach($subModulos as $subModulo)
                                            @if($permisos->where("IdModulo", $subModulo->IdModulo)->isNotEmpty())
                                                <li class="dropdown-submenu">
                                                    <a>
                                                        {!! $subModulo->Nombre !!}
                                                        <i class="dropdown-icon"></i>
                                                    </a>
                                                    <ul class="dropdown-menu">

                                                @foreach($permisos->where("IdModulo", $subModulo->IdModulo)->all() as $opcion)
                                                    <li>
                                                        <a class="seleccionOpcionMenu" data-urlvista="{!! $opcion->URL !!}" data-nombrefuncionalidad="{!! $opcion->Nombre !!}">
                                                            <i class="fa {!! $opcion->FaIconDesc!!}"></i>
                                                            {!! $opcion->Nombre !!}
                                                        </a>
                                                    </li>
                                                @endforeach
                                                    </ul>
                                                </li>
                                            @endif
                                        @endforeach


                                        @foreach ($permisos->where("IdModulo", $modulo->IdModulo)->all() as $opcion)
                                            <li>
                                                <a class="seleccionOpcionMenu" data-urlvista="{!! $opcion->URL !!}" data-idfuncionalidad="{!! $opcion->ContentHtmlId !!}" data-nombrefuncionalidad="{!! $opcion->Nombre !!}">
                                                    <i class="fa {!! $opcion->FaIconDesc!!}"></i>
                                                    {!! $opcion->Nombre !!}
                                                </a>
                                            </li>
                                        @endforeach
                                    </ul>
                                </li>
                            @endif
                        @endforeach
                    </ul>
                </nav>
            </div>
        </div>
        <div class="content">
            <div id="divCuerpoMaster" class="container">
                @yield("CuerpoMaster")
            </div>
        </div>
        @yield("Mensajes")
    </div>
    <footer class="footer">
        <div class="container">
            <p class="pull-left">Copyright &copy; 2018 GMGI-RSILMAN.</p>
        </div>
    </footer>
</body>
</html>
